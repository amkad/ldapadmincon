#!/usr/local/bin/perl -w


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################


{

    package Actions::ldapUserActions;
    use strict;
    use Utils qw(basename fgrep);
    use PlugIns::UserAdmin;
    use PlugIns::GroupAdmin;
    use Screens;

    our $params;
    our $Uadmin;
    our $Gadmin;
    our %opts;

    sub new {
        my $proto = shift;
        %opts = @_;

        my $class = ref($proto) || $proto;
        my $self;
        eval { $self = $class->SUPER::new(); };

        $self->{adduser}   = \&adduser;
        $self->{deluser}   = \&deluser;
        $self->{moduser}   = \&moduser;
        $self->{lockuser}  = \&lockuser;
        $self->{passreset} = \&passreset;

        bless( $self, $class );

        $params = $main::params;
        $Uadmin = PlugIns::UserAdmin->new();
        $Gadmin = PlugIns::GroupAdmin->new();

        return $self;
    }

    sub ValidFormData(@) {
        my $self = shift;

        my $username  = $params->{username};
        my $firstname = $params->{firstname};
        my $lastname  = $params->{lastname};
        my $homedir   = $params->{homedir};
        my $email     = $params->{email};
        my $uidnum    = $params->{uid};
        my $gidnum    = $params->{gid};
        my $shell     = $params->{shell};

        my %allusers = $Uadmin->AllUsers();

        if ( ( !$username ) || ( $username !~ m/^[A-Za-z][A-Za-z0-9_\-.]{0,13}$/ ) )
        {
            push @main::errors, "Invalid username: $username";
        }

        if (    $username
             && exists( $allusers{$username} )
             && $params->{action} ne "moduser" )
        {
            push @main::errors, "Username already exists: $username";
        }

        if ( ( !$lastname ) || ( !$lastname =~ m/^[A-Za-z\_\-\ ]{1,40}$/ ) ) {
            push @main::errors, "Invalid Last name: $lastname";
        }

        if ( scalar @main::errors > 0 ) { return undef; }
        return 1;
    }

    sub adduser(@) {
        my $self = shift;

        unless ( $self->ValidFormData() ) {
            print Screens::Status();
            exit 1;
        }

        eval {
            $Uadmin->AddUser(
                              username       => $params->{username},
                              first          => $params->{firstname},
                              last           => $params->{lastname},
                              home           => $params->{homedir},
                              email          => $params->{email},
                              uidnum         => $params->{uid},
                              gidnum         => $params->{gid},
                              shell          => $params->{shell},
                              shadowMin      => $params->{shadowmin},
                              shadowMax      => $params->{shadowmax},
                              shadowWarning  => $params->{shadowwarning},
                              shadowInactive => $params->{shadowinactive},
                              shadowExpire   => $params->{shadowexpire},
                            );
        };
        push @main::errors, $@ if ($@);

        unless ( scalar @main::errors > 0 ) {
            if ( $params->{ldapadmin} && $params->{ldapadmin} eq "on" ) {
                eval { $Uadmin->LdapAdmin( $params->{username}, 'true' ); };
                push @main::warnings, $@ if ($@);
            }
            else {
                eval { $Uadmin->LdapAdmin( $params->{username}, 'false' ); };
                push @main::warnings, $@ if ($@);
            }

            if ( $params->{temppass} && $params->{temppass} eq "on" ) {
                my $temporary_password = $self->TemporaryPassword();
                eval {
                    $Uadmin->ChangeAndEmailPassword( $params->{username},
                                                     $temporary_password );
                };
                if ($@) {
                    push @main::warnings, $@;
                }
                else {
                    push @main::messages,
"User: $params->{username}, Temporary Password: $temporary_password";
                }
            }
        }
        $main::action_string = "Added user $params->{username}";
        print Screens::Status();
        exit 0;
    }

    sub deluser(@) {
        my $self     = shift;
        my $username = $params->{username};

        my %allusers = $Uadmin->AllUsers();

        if ( ( !$username ) || ( $username !~ m/^[A-Za-z][A-Za-z0-9_\-.]{0,13}$/ ) )
        {
            push @main::errors, "Invalid username: $username";
        }

        if ( $username && !exists( $allusers{$username} ) ) {
            push @main::errors, "Username does not exist: $username";
        }

        if ( scalar @main::errors > 0 ) {
            print Screens::Status();
            exit 1;
        }

        eval { $Uadmin->DeleteUser( $params->{username} ); };
        push @main::errors, $@ if ($@);

        $main::action_string = "Deleted user $params->{username}";
        print Screens::Status();
        exit 0;
    }

    sub moduser(@) {
        my $self = shift;

        unless ( $self->ValidFormData() ) {
            print Screens::Status();
            exit 1;
        }

        eval {
            $Uadmin->ModUser(
                              username       => $params->{username},
                              first          => $params->{firstname},
                              last           => $params->{lastname},
                              home           => $params->{homedir},
                              email          => $params->{email},
                              uidnum         => $params->{uid},
                              gidnum         => $params->{gid},
                              shell          => $params->{shell},
                              shadowMin      => $params->{shadowmin},
                              shadowMax      => $params->{shadowmax},
                              shadowWarning  => $params->{shadowwarning},
                              shadowInactive => $params->{shadowinactive},
                              shadowExpire   => $params->{shadowexpire},
                            );
        };
        push @main::errors, $@ if ($@);

        unless ( scalar @main::errors > 0 ) {
            if ( $params->{ldapadmin} && $params->{ldapadmin} eq "on" ) {
                eval { $Uadmin->LdapAdmin( $params->{username}, 'true' ); };
                push @main::warnings, $@ if ($@);
            }
            else {
                eval { $Uadmin->LdapAdmin( $params->{username}, 'false' ); };
                push @main::warnings, $@ if ($@);
            }
        }
        $main::action_string = "Modified user $params->{username}";
        print Screens::Status();
        exit 0;
    }

    sub lockuser(@) {
        my $self = shift;
        my @userlist;

        if ( ref( $params->{selected_userlist} ) ) {
            @userlist = @{ $params->{selected_userlist} };
        }
        else {
            push @userlist, $params->{selected_userlist};
        }

        for my $user (@userlist) {
            $user =~ s/\ ------\ .*//g;
            eval { $Uadmin->UpdateUserAttr( $user, userPassword => [undef] ); };
            if ($@) {
                push @main::errors, "User: $user: $@";
            }
            else {
                push @main::messages, "User: $user; account Locked";
            }
        }
        $main::action_string = 'Users locked';
        print Screens::Status();
        exit 0;
    }

    sub TemporaryPassword(@) {
        my $self = shift;

        my $temporary_password;
        for ( my $i = 0 ; $i < 8 ; $i++ ) { $temporary_password .= 'X'; }

        my $randchar = sub {
            my @chars;
            for ( my $i = 48 ; $i < 122 ; $i++ ) {
                if (    ( $i >= 57 ) && ( $i <= 64 )
                     || ( $i >= 91 ) && ( $i <= 96 ) )
                {
                    next;
                }
                push @chars, pack( 'C', $i );
            }
            my $charnum = int( rand($#chars) );
            return $chars[$charnum];
        };

        $temporary_password =~ s/(X)(?=X|$)/&$randchar/eg;
        return $temporary_password;
    }

    sub passreset(@) {
        my $self = shift;
        my @userlist;
        if ( ref( $params->{selected_userlist} ) ) {
            @userlist = @{ $params->{selected_userlist} };
        }
        else {
            push @userlist, $params->{selected_userlist};
        }

        for my $user (@userlist) {
            $user =~ s/\ ------\ .*//g;

            my $temporary_password = $self->TemporaryPassword();
            eval {
                $Uadmin->ChangeAndEmailPassword( $user, $temporary_password );
            };
            if ($@) {
                push @main::errors, "User: $user, $temporary_password: $@";
            }
            else {
                push @main::messages,
                  "User: $user, Temporary Password: $temporary_password";
            }
        }
        $main::action_string = 'Passwords reset';
        print Screens::Status();
        exit 0;
    }
}
1;
