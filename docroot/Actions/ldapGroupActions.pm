#!/usr/local/bin/perl -w


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################


{

    package Actions::ldapGroupActions;
    use strict;
    use Utils qw(basename fgrep);
    use PlugIns::UserAdmin;
    use PlugIns::GroupAdmin;
    use Screens;

    our $params;
    our $Uadmin;
    our $Gadmin;
    our %opts;

    sub new {
        my $proto = shift;
        %opts = @_;

        my $class = ref($proto) || $proto;
        my $self;
        eval { $self = $class->SUPER::new(); };

        $self->{addgroup}      = \&addgroup;
        $self->{delgroup}      = \&delgroup;
        $self->{modgroup}      = \&modgroup;
        $self->{changemembers} = \&changemembers;

        bless( $self, $class );

        $params = $main::params;
        $Uadmin = PlugIns::UserAdmin->new();
        $Gadmin = PlugIns::GroupAdmin->new();

        return $self;
    }

    sub ValidFormData(@) {
        my $self = shift;

        my $groupname = $params->{groupname};
        my $gidnum    = $params->{gid};

        my %allgroups = $Gadmin->AllGroups();

        if (    ( !$groupname )
             || ( $groupname !~ m/^[A-Za-z][A-Za-z0-9-]+$/ ) )
        {
            push @main::errors, "Invalid groupname: $groupname";
        }

        if (    $groupname
             && exists( $allgroups{$groupname} )
             && $params->{action} ne "modgroup" )
        {
            push @main::errors, "Groupname already exists: $groupname";
        }

        if ( scalar @main::errors > 0 ) { return undef; }
        return 1;
    }

    sub addgroup(@) {
        my $self      = shift;
        my $groupname = $params->{groupname};
        my $gid       = $params->{gid};

        unless ( $self->ValidFormData() ) {
            print Screens::Status();
            exit 1;
        }

        eval { $Gadmin->AddGroup( groupname => $groupname, gidnum => $gid ); };
        push @main::errors, $@ if ($@);

        $main::action_string = "$groupname added";
        print Screens::Status();
        exit 0;
    }

    sub modgroup(@) {
        my $self      = shift;
        my $groupname = $params->{groupname};
        my $gid       = $params->{gid};

        unless ( $self->ValidFormData() ) {
            print Screens::Status();
            exit 1;
        }

        eval { $Gadmin->UpdateGroupAttr( $groupname, gidNumber => $gid ); };
        push @main::errors, $@ if ($@);

        $main::action_string = "$groupname modified";
        print Screens::Status();
        exit 0;
    }

    sub delgroup(@) {
        my $self      = shift;
        my $groupname = $params->{groupname};

        eval { $Gadmin->DeleteGroup($groupname); };

        $main::action_string = "$groupname deleted";
        print Screens::Status();
        exit 0;
    }

    sub changemembers(@) {
        my $self = shift;
        my @userlist;

        my $added_users;
        my $removed_users;

        if ( ref( $params->{selected_userlist} ) ) {
            @userlist = @{ $params->{selected_userlist} };
        }
        else {
            push @userlist, $params->{selected_userlist};
        }
        for (@userlist) {
            $_ =~ s/\ ------\ .*//g;
        }

        eval {
            ( $added_users, $removed_users ) =
              $Gadmin->ChangeMembers( $params->{groupname},
                                      userlist => \@userlist );
        };
        if ($@) {
            push @main::errors, "$@";
        }
        else {
            for (@$added_users) {
                push @main::messages, "Added $_ to group $params->{groupname}";
            }
            for (@$removed_users) {
                push @main::messages,
                  "Removed $_ from group $params->{groupname}";
            }
        }

        $main::action_string = 'Group memberships updated';
        print Screens::Status();
        exit 0;
    }
}
1;
