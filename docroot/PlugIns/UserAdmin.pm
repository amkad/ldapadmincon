#!/usr/local/bin/perl -w


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################



require "config.ph";

{
    $main::plugins{Users}->{Screen}  = 'ldapUserAdminScreen';
    $main::plugins{Users}->{Actions} = 'ldapUserActions';
    $main::plugins{Users}->{index}   = 0;

    die "attempt to load UserAdmin from non Admin account " . caller() . "\n"
      unless (    $main::params->{userisAdmin}
               && $main::params->{userisAdmin} eq "true" );

    package PlugIns::UserAdmin;
    use LDAPtools qw/BindLDAP makeDN UpdateAttr parseURI/;
    use Utils qw(basename);
    use Net::LDAP;
    use Net::LDAPS;
    use strict;
    use Data::Dumper;
    use POSIX;
    use Email;
    use Encode;

    our $ldap;
    our %opts;

    sub new {
        my $proto = shift;
        %opts = @_;

        my $class = ref($proto) || $proto;
        my $self;
        eval { $self = $class->SUPER::new(); };

        $ldap = $opts{ldap} || BindLDAP();

        $self->{ChangePassword} = undef;
        $self->{UpdateUserAttr} = undef;

        bless( $self, $class );

        return $self;
    }

    sub ChangePassword(@) {
        my $self         = shift;
        my $username     = shift;
        my $new_password = shift;
        my %args         = @_;
        my $mesg;

        chomp( my $crypted_passwd =
			`$main::config->{slappasswd} -h $main::config->{cryptmethod} -s '$new_password'`
        );

        my $dn = makeDN($username);

        if ($ldap) {

   # Reset shadowLastChange to 0 to cause user to be prompted to change password
   # when they next log in (Also allows a user to change their password after
   # forgetting it, and having it reset within shadowMin days
            $mesg = $ldap->modify(
                             $dn,
                             changes => [
                                 replace => [ userPassword => $crypted_passwd ],
                             ]
            );

            $mesg->code() && die $mesg->error() . "\n";
            return 1;
        }
    }

    sub UpdateUserAttr(@) {
        my $self     = shift;
        my $username = shift;
        my %args     = @_;

        my $dn = makeDN($username);
        return UpdateAttr(
                           $dn, $ldap,
                           {
                             base   => $main::config->{base_passwd},
                             filter => "(uid=$username)"
                           },
                           \%args
                         );
    }

    sub IndividualShadowUpdate(@) {
        my $self             = shift;
        my $username         = shift;
        my %args             = @_;
        my $shadowLastChange = $args{shadowLastChange};
        my $shadowMin        = $args{shadowMin} || [undef];
        my $shadowMax        = $args{shadowMax} || [undef];
        my $shadowWarning    = $args{shadowWarning} || [undef];
        my $shadowInactive   = $args{shadowInactive} || [undef];
        my $shadowExpire     = $args{shadowExpire} || [undef];

        if ($ldap) {
            $self->UpdateUserAttr(
                                   $username,
                                   shadowLastChange => $shadowLastChange,
                                   shadowMin        => $shadowMin,
                                   shadowMax        => $shadowMax,
                                   shadowWarning    => $shadowWarning,
                                   shadowInactive   => $shadowInactive,
                                   shadowExpire     => $shadowExpire,
                                 );
            return 1;
        }
    }

## TODO:
# This routine currently has no UI; need to create UI to perform global shadow updates

    sub AllShadowUpdate(@) {
        my $self             = shift;
        my %args             = @_;
        my $shadowLastChange = $args{shadowLastChange};
        my $shadowMin        = $args{shadowMin};
        my $shadowMax        = $args{shadowMax};
        my $shadowWarning    = $args{shadowWarning};
        my $shadowInactive   = $args{shadowInactive};
        my $shadowExpire     = $args{shadowExpire};

        my $mesg = $ldap->search( base   => $main::config->{base_passwd},
                                  filter => '(uid=*)' );

        for my $entry ( $mesg->all_entries() ) {
            my $username = $entry->get_value('uid');
            next if ( $username eq "root" );
            $self->IndividualShadowUpdate(
                                          $username,
                                          shadowLastChange => $shadowLastChange,
                                          shadowMin        => $shadowMin,
                                          shadowMax        => $shadowMax,
                                          shadowWarning    => $shadowWarning,
                                          shadowInactive   => $shadowInactive,
                                          shadowExpire     => $shadowExpire,
                                         );
        }
    }

    sub DeleteUser(@) {
        my $self     = shift;
        my $username = shift;
        my %args     = @_;

        my $userexists = undef;

        my $dn = makeDN($username);

        my $mesg = $ldap->compare( $dn, attr => 'uid', value => $username );
        if ( $mesg->code() == 6 ) {
            $userexists = 1;
        }

        if ($userexists) {
            $mesg = $ldap->delete("uid=$username,$main::config->{base_passwd}");
            $mesg->code() && die $mesg->error() . "\n";
            return 1;
        }
        die "Delete user failed, user does not seem to exist" . "\n";
    }

    sub LockUser(@) {
        my $self       = shift;
        my $username   = shift;
        my %args       = @_;
        my $userexists = undef;

        my $dn = makeDN($username);

        my $mesg = $ldap->compare( $dn, attr => 'uid', value => $username );
        if ( $mesg->code() == 6 ) {
            $userexists = 1;
        }

        if ($userexists) {
            my $dn = makeDN($username);
            $mesg = $ldap->modify( $dn, delete => 'userPassword' );

            $mesg->code() && die $mesg->error() . "\n";
            return 1;
        }
        die "Lock user failed, user does not seem to exit" . "\n";
    }

    sub AddUser(@) {
        my $self = shift;
        my %args = @_;

        my $days_since_epoch = POSIX::ceil( time() / 60 / 60 / 24 );

        my $dn = makeDN( $args{username} );

        my $mesg =
          $ldap->compare( $dn, attr => 'uid', value => $args{username} );
        if ( $mesg->code() == 6 ) {
            die "Add user failed, user $args{username} already exists" . "\n";
        }

        die 'Insufficient Attributes provides' . "\n"
          unless (    $args{username}
                   && $args{last}
                   && $args{first}
                   && $args{email} );
        $mesg = $ldap->add(
                            "uid=$args{username},$main::config->{base_passwd}",
                            attr => [
                                    objectClass => [
                                        'top',                  'person',
                                        'organizationalPerson', 'inetOrgPerson',
                                        'LDAPAdmin'
                                                   ],
                                    uid           => $args{username},
                                    cn            => "$args{first} $args{last}",
                                    sn            => $args{last},
                                    givenName     => $args{first},
                                    mail          => "$args{email}",
                            ]
                          );
        $mesg->code() && die $mesg->error() . "\n";
        return 1;
    }

    sub ModUser(@) {
        my $self = shift;
        my %args = @_;
        my $mesg;

        my $dn = makeDN( $args{username} );

        die 'Insufficient Attributes provides' . "\n"
          unless (    $args{username}
                   && $args{last}
                   && $args{first}
                   && $args{email} );
        $self->UpdateUserAttr(
                               $args{username},
                               cn            => "$args{first} $args{last}",
                               sn            => $args{last},
                               givenName     => $args{first},
                               mail          => "$args{email}",
                             );

        return 1;
    }

## TODO:
# Need to update tempate with a magic button to populate the "uid" field with the next
# available UID; this function will likely not be used, and instead this will be
# handled natively in javascript.

    sub getNextUID(@) {
        my $self = shift;
        my %args = @_;
        my %UIDs;
        my $nextUID;
        my $mesg = $ldap->search( base   => $main::config->{base_passwd},
                                  filter => '(uid=*)' );

        for my $entry ( $mesg->all_entries() ) {
            $UIDs{ $entry->get_value('uidNumber') } = 1;
        }

        for ( my $i = $main::config->{lowestUID} || 1000 ; $i < 131072 ; $i++ )
        {
            if ( exists $UIDs{$i} ) {
                next;
            }
            return $i;
        }
    }

    sub validUid(@) {
        my $self = shift;
        my $uid  = shift;
        my %args = @_;

        my $mesg = $ldap->search( base   => $main::config->{base_passwd},
                                  filter => '(uid=*)' );

        for my $entry ( $mesg->all_entries() ) {
            return undef if ( $entry->get_value('uid') eq $uid );
        }
        return 1;
    }

## TODO:
# Add Group admin attributes (requires schema definition)
# This will also require an addition method in this class to add "host: " attribute
# to all users in a specific group; as padl libraries don't pay attention to "host: "
# attributes in group memberships (rather annoying if you ask me)
# may elect to keep this in the upcoming ACL module, as managing which users
# are permitted to log in to which host is much more reliably enforced via ACL
# than through "host: " attribute management.

# Group admins would be able to manage users for a group of hosts, or for their
# orgizational unit; this would be 3 types of Group admin, "none", which would
# be the same as not having LDAPgroupAdmin undefined; "host", which would be
# able only to add/remove users to/from a host group (and only to a host group they
# admin); they would NOT be able to add new users, or delete users from the directory.
# and the third would be "org"; which would be an admin permitted to add/delete users
# from the ldap directory, but users added by them would only be permitted login
# to servers in their org group's control; my hope is that these 3 models would
# support _most_ users requirements for distributing management of LDAP directories.

    # Long todo section...

    sub LdapAdmin(@) {
        my $self       = shift;
        my $username   = shift;
        my $true_false = shift;
        my %args       = @_;

        $self->UpdateUserAttr( $username, LDAPadmin => $true_false );
        return 1;
    }

    sub AllUsers(@) {
        my $self = shift;
        my %args = @_;
        my %rethash;

        my $mesg = $ldap->search( base   => $main::config->{base_passwd},
                                  filter => '(uid=*)' );

        for my $entry ( $mesg->all_entries() ) {
            my $username = $entry->get_value('uid');
            $rethash{$username} = {
                                    firstname => $entry->get_value('givenName'),
                                    lastname  => $entry->get_value('sn'),
                                  };
            my $gecos;
            my $LDAPadmin;
            my $email;

            $rethash{$username}->{locked} = 1
              if ( !$entry->exists('userPassword') );

            $rethash{$username}->{gecos} = $gecos
              if ( $gecos = $entry->get_value('cn') );
            $rethash{$username}->{LDAPadmin} = $LDAPadmin
              if ( $LDAPadmin = $entry->get_value('LDAPadmin') );
            $rethash{$username}->{email} = $email
              if ( $email = $entry->get_value('mail') );
        }
        return %rethash;
    }

    sub CreateHomedir(@) {
        my $self        = shift;
        my $uid         = shift;
        my $gid         = shift;
        my $homedir     = shift;
        my $session_key = $main::params->{session_id};
        my $ssid        = $main::params->{ssid};

        $uid =~ m/^([0-9]+)$/;
        $uid = $1;

        $gid =~ m/^([0-9]+)$/;
        $gid = $1;

        $homedir =~ m/^(\/[A-Za-z0-9\_\-\/]+)$/;
        $homedir = $1;

        $session_key =~ m/^([A-Za-z0-9]+)$/;
        $session_key = $1;

        $ssid =~ m/^([A-Za-z0-9\%]+)$/;
        $ssid = $1;

        if ( ( !$homedir ) || ( $homedir !~ m/^\// ) ) {
            return undef;
        }
        else {
            my $bn = basename($homedir);
            ( my $home_base = $homedir ) =~ s/$bn$//g;
            if ( !-d $home_base ) {
                return undef;
            }
        }
## TODO:
        # Pass session_key and ssid via $ENV rather than on the command line

        system(
            "/etc/LDAPadmin/mkhomedir $uid $gid $homedir $session_key '$ssid'");
        return undef;
    }

    sub SkelHomedir(@) {
        my $self        = shift;
        my $uid         = shift;
        my $gid         = shift;
        my $homedir     = shift;
        my $session_key = $main::params->{session_id};
        my $ssid        = $main::params->{ssid};

        $uid =~ m/^([0-9]+)$/;
        $uid = $1;

        $gid =~ m/^([0-9]+)$/;
        $gid = $1;

        $homedir =~ m/^(\/[A-Za-z0-9\_\-\/]+)$/;
        $homedir = $1;

        $session_key =~ m/^([A-Za-z0-9]+)$/;
        $session_key = $1;

        $ssid =~ m/^([A-Za-z0-9\%]+)$/;
        $ssid = $1;

        return undef unless ( -d $homedir );
        return undef
          unless ( $main::config->{skel_dir} && -d $main::config->{skel_dir} );

      # TODO: Pass session_key and ssid via $ENV rather than on the command line
        system(
            "/etc/LDAPadmin/skelhomedir $uid $gid $homedir $session_key '$ssid'"
        );
        return undef;
    }

    sub ChangeAndEmailPassword(@) {
        my $self      = shift;
        my $username  = shift;
        my $temp_pass = shift;
        my $message   = [];
        my $email;

        my $mesg = $ldap->search( base   => $main::config->{base_passwd},
                                  filter => "(uid=$username)" );

        for my $entry ( $mesg->all_entries() ) {
            $email = $entry->get_value('mail');
        }

        $self->ChangePassword( $username, $temp_pass ) || return undef;

        open( MESSAGE, "< $main::config->{email_message}" );
        while (<MESSAGE>) {
            chomp;
            s/\[\[username\]\]/$username/ge;
            s/\[\[password\]\]/$temp_pass/ge;
            push @{$message}, $_;
        }
        close(MESSAGE);

        if ($email) {
            my $mail = Email->new( Server => $main::config->{smtp_server},
                                   Host   => $main::config->{hostname} );
            $mail->SendMessage(
                                subject => $main::config->{email_subject},
                                to      => [$email],
                                from => 'noreply@' . $main::config->{hostname},
                                message => $message
                              );
        }
    }
}

1;
