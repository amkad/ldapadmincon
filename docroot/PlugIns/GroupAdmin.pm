#!/usr/local/bin/perl -w


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################



require "config.ph";

{
    $main::plugins{Groups}->{Screen}  = 'ldapGroupAdminScreen';
    $main::plugins{Groups}->{Actions} = 'ldapGroupActions';
    $main::plugins{Groups}->{index}   = 1;

    die "attempt to load GroupAdmin from non Admin account " . caller() . "\n"
      unless (    $main::params->{userisAdmin}
               && $main::params->{userisAdmin} eq "true" );

    package PlugIns::GroupAdmin;
    use LDAPtools qw/BindLDAP UpdateAttr parseURI/;
    use Net::LDAP;
    use Net::LDAPS;
    use strict;
    use Data::Dumper;
    use POSIX;

    our $ldap;
    our %opts;

    sub new {
        my $proto = shift;
        %opts = @_;

        my $class = ref($proto) || $proto;
        my $self;
        eval { $self = $class->SUPER::new(); };

        $ldap = $opts{ldap} || BindLDAP();

        $self->{UpdateGroupAttr} = undef;

        bless( $self, $class );

        return $self;
    }

    sub makeDN(@) {
        my $groupname = shift;
        return "cn=$groupname," . $main::config->{base_group};
    }

    sub UpdateGroupAttr(@) {
        my $self      = shift;
        my $groupname = shift;
        my %args      = @_;

        my $dn = makeDN($groupname);
        return UpdateAttr(
                           $dn, $ldap,
                           {
                             base   => $main::config->{base_group},
                             filter => "(cn=$groupname)"
                           },
                           \%args
                         );
    }

    sub ChangeMembers(@) {
        my $self      = shift;
        my $groupname = shift;
        my %args      = @_;
        my %members;
        my %in_memberlist;
        my @added_users;
        my @removed_users;

        map { $in_memberlist{$_} = 1 } @{ $args{userlist} };

        my $dn = makeDN($groupname);
        my $mesg = $ldap->search( base   => $main::config->{base_group},
                                  filter => "(cn=$groupname)" );

        $mesg->code() && die $mesg->error() . "\n";

        for my $entry ( $mesg->all_entries() ) {
            map { $members{$_} = 1 } $entry->get_value('memberUid');
        }

        for ( keys %in_memberlist ) {
            next if ( exists( $members{$_} ) );
            my $userdn = LDAPtools::makeDN($_);
            my $mesg = $ldap->compare( $userdn, attr => 'uid', value => $_ );
            if ( $mesg->code() == 6 ) {
                my $mesg = $ldap->modify( $dn, add => { memberUid => $_ } );
                $mesg->code() && die "Unable to add " . $_ . " into " . $groupname . ": " . $mesg->error() . "\n";
                push @added_users, $_;
            }
        }
        for ( keys %members ) {
            next if ( exists( $in_memberlist{$_} ) );
            my $mesg = $ldap->modify( $dn, delete => { memberUid => $_ } );
            $mesg->code() && die "Unable to delete " . $_ . " from " . $groupname . ": " . $mesg->error() . "\n";
            push @removed_users, $_;
        }
        return ( \@added_users, \@removed_users );
    }

    sub DeleteGroup(@) {
        my $self      = shift;
        my $groupname = shift;
        my %args      = @_;

        my $groupexists = undef;

        my $dn = makeDN($groupname);

        my $mesg = $ldap->compare( $dn, attr => 'cn', value => $groupname );
        if ( $mesg->code() == 6 ) {
            $groupexists = 1;
        }

        if ($groupexists) {
            $mesg = $ldap->delete($dn);
            $mesg->code() && die $mesg->error() . "\n";
            return 1;
        }
        die "Delete Group failed, user does not seem to exist" . "\n";
    }

    sub AddGroup(@) {
        my $self = shift;
        my %args = @_;

        my $dn = makeDN( $args{groupname} );

        my $mesg =
          $ldap->compare( $dn, attr => 'cn', value => $args{groupname} );
        if ( $mesg->code() == 6 ) {
            die "Add Group failed, user $args{groupname} already exists" . "\n";
        }

        die 'Insufficient Attributes provides' . "\n"
          unless ( $args{groupname} );

        $mesg = $ldap->add(
                            "cn=$args{groupname},$main::config->{base_group}",
                            attr => [
                                      objectClass => [ 'top', 'posixGroup' ],
                                      cn          => $args{groupname},
                                      gidNumber   => -1,
                                    ]
                          );
        $mesg->code() && die $mesg->error() . "\n";

        return 1;
    }

    sub getNextGID(@) {
        my $self = shift;
        my %args = @_;
        my %GIDs;
        my $nextGID;
        my $mesg = $ldap->search( base   => $main::config->{base_group},
                                  filter => '(cn=*)' );

        for my $entry ( $mesg->all_entries() ) {
            $GIDs{ $entry->get_value('gidNumber') } = 1;
        }

        for ( my $i = 0 ; $i < 131072 ; $i++ ) {
            if ( exists $GIDs{$i} ) {
                next;
            }
            return $i;
        }
    }

    sub validNewGid(@) {
        my $self = shift;
        my $gid  = shift;
        my %args = @_;

        my $mesg = $ldap->search( base   => $main::config->{base_group},
                                  filter => '(cn=*)' );

        for my $entry ( $mesg->all_entries() ) {
            return undef if ( $entry->get_value('gidNumber') eq "$gid" );
        }
        return 1;
    }

    sub validGid(@) {
        my $self = shift;
        my $gid  = shift;
        my %args = @_;

        my $mesg = $ldap->search( base   => $main::config->{base_group},
                                  filter => '(cn=*)' );

        for my $entry ( $mesg->all_entries() ) {
            return 1 if ( $entry->get_value('gidNumber') eq "$gid" );
        }
        return undef;
    }

    sub AllGroups(@) {
        my $self = shift;
        my %args = @_;
        my %rethash;

        my $mesg = $ldap->search( base   => $main::config->{base_group},
                                  filter => '(cn=*)' );

        for my $entry ( $mesg->all_entries() ) {
            my $groupname = $entry->get_value('cn');

            @{ $rethash{$groupname}->{users} } = $entry->get_value('memberUid');
        }
        return %rethash;
    }
}

1;
