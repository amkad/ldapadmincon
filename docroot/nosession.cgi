#!/usr/bin/perl -w


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################


{

    package main;
    our $config;

    BEGIN {
        require "/etc/LDAPadmin/config.ph";
    }
    use CGI qw(:utf8);
    our %plugins;
    our $params;
    our $cgi;
    our $expired = undef;

    use strict;
    use Screens;
    use Modules qw(Use Load);

    $cgi = CGI->new();
    $cgi->charset('UTF-8');

    sub vars() {
        my $subparams;
        foreach my $param ( $cgi->param() ) {
            my @values = $cgi->param($param);

            if ( scalar(@values) > 1 ) {
                push( @{ $subparams->{$param} }, $cgi->param($param) );
            }
            else {
                $subparams->{$param} = $cgi->param($param);
            }
        }

        if ( $ENV{QUERY_STRING} ) {
            my @args = split( '&', $ENV{QUERY_STRING} );
            for (@args) {
                chomp;
                my ( $key, $val ) = split('=');
                $key =~ /authusername/ && next;
                $key =~ /password/     && next;
                $subparams->{$key} = $val;
            }
        }
        return $subparams;
    }

    $params = vars();

    opendir( PLUGINS, $main::config->{install_dir} . "/NoSessionPlugIns/" )
      || die "can't open plugins\n";
    for ( readdir(PLUGINS) ) {
        $_ =~ s/(.*)/$1/g;
        /^.{1,2}$/ && next;
        /^.*\.pm$/ && do {
            s/\.pm$//;
            Use("NoSessionPlugIns::$_");
          }
    }

    print $cgi->header();

    unless ( $params->{action} && $params->{screen} ) {
        $params->{screen} =
          ( exists $params->{screen} ) ? $params->{screen} : 'User';
        print &{ \&{ "Screens::"
                  . $plugins{ $params->{screen} }->{Screen}
                  . "()" } };
    }
    else {
        my $screen = $params->{screen};
        my $action = $params->{action};

        my $act_handle =
          Load( 'NoSessionActions::' . $plugins{$screen}->{Actions} );
        if ( exists( $act_handle->{$action} ) ) {
            $act_handle->{$action}($act_handle);
        }
    }
}

