#!/usr/local/bin/perl -w


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################



require "config.ph";

{
    $main::plugins{User}->{Screen}  = 'NoSessionUserScreen';
    $main::plugins{User}->{Actions} = 'NoSessionUserActions';
    $main::plugins{User}->{index}   = 0;

    package NoSessionPlugIns::User;
    use LDAPtools qw(BindLDAP makeDN);
    use LDWSession;
    use Email;
    use CGI qw(:utf8);
    use POSIX;

    our $ldap;
    our %opts;
    our $cgi;

    sub new {
        my $proto = shift;
        %opts = @_;

        my $class = ref($proto) || $proto;
        my $self;
        eval { $self = $class->SUPER::new(); };

        $ldap = $opts{ldap} || BindLDAP();
        *cgi  = \$main::cgi;

        $self->{CreateAnonSession} = undef;

        bless( $self, $class );

        return $self;
    }

    sub CreateAnonSession(@) {
        my $self     = shift;
        my $username = shift;
        my $in_email = shift;
        my $user;
        my $session_key;
        my $ssid;

        die "No valid username or email address provided"
          unless ( $username && $in_email ) . "\n";

        my $mesg = $ldap->search( base   => $main::config->{base_passwd},
                                  filter => "(uid=$username)" );

        if ( $mesg->code() ) {
            die "Error message: " . $mesg->error() . "\n";
        }

        my $days_since_epoch = POSIX::ceil( time() / 60 / 60 / 24 );

        my %shadow;
        my $locked = undef;
        for my $entry ( $mesg->all_entries() ) {
            $shadow{LastChange} = $entry->get_value('shadowLastChange');
            $shadow{Min}        = $entry->get_value('shadowMin') || 0;
            $shadow{Max}        = $entry->get_value('shadowMax') || 99999;
            $shadow{Inactive}   = $entry->get_value('shadowInactive') || 99999;
            $shadow{Expire}     = $entry->get_value('shadowExpire') || undef;
            $user               = $entry->get_value('uid');
            $email              = $entry->get_value('mail');
            $locked             = 1
              if ( $entry->exists('uid') && !$entry->exists('userPassword') );
        }

        if (    $shadow{LastChange}
             && $days_since_epoch >=
             ( $shadow{LastChange} + $shadow{Max} + $shadow{Inactive} ) )
        {
            die "Account locked, contact system administrator\n";
        }

        if (    $shadow{LastChange}
             && $shadow{Expire}
             && $days_since_epoch >= $shadow{Expire} )
        {
            die "Account expired, contact system administrator\n";
        }

        if ($locked) {
            die "Account locked, contact system administrator\n";
        }

        die "User or Email invalid\n" unless ( $user  && $user  eq $username );
        die "User or Email invalid\n" unless ( $email && $email eq $in_email );

        my $ses = LDWSession->new();

        open( NULLSTATE, "<", undef );
        $cgi = CGI->new( \*NULLSTATE );    #ensure params are empty!
        $cgi->charset('UTF-8');

        $session_key = $ses->create_session();
        $ssid        = $ses->create_ssid();
        $cgi->param( -name => 'authusername', -value => $user );
        $cgi->param( -name => 'password',     -value => '--removed--' );
        $cgi->param( -name => 'ssid',         -value => $ssid );
        $cgi->param( -name => 'sessiontime',  -value => time() );
        $cgi->param( -name => 'state',        -value => undef );
        $ses->save_state($session_key);

        my $mail = Email->new( Server => $main::config->{smtp_server},
                               Host   => $main::config->{hostname} );
        $mail->SendMessage(
            subject => 'LDAP Account management',
            to      => [$email],
            from    => 'noreply@' . $main::config->{hostname},
            message => [
                'This is an automated message',
                '',
'Please select, or copy/paste into your browser, the link below:',
                'https://www.'
                  . $main::config->{hostname}
                  . $main::config->{docroot}
                  . '/index.cgi?session_id='
                  . $session_key
                  . '&ssid='
                  . $ssid,
                'where you will be able to change your password.'
            ],
        );
    }
}
1;
