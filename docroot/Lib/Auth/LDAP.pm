#!/usr/local/bin/perl -w


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################


use strict;

require "/opt/webmin/config.ph";
{

    package Auth::LDAP;

    #require "Auth.pm";
    use Net::LDAP;
    use Net::LDAPS;
    use Data::Dumper;

    #our @ISA = qw(Auth);
    sub parseURI(@) {
        my $URI = shift;
        $URI =~ m!(ldaps?)://(.*?):(\d{1,7})/?!;
        return ( $1, $2, $3 );
    }

    sub Authentication(@) {
        my $self     = shift;
        my $username = shift;
        my $password = shift;

        my ( $proto, $hostname, $port ) = parseURI("$main::config->{uri}");

        return undef unless ( $password && $username );
        if ( exists( $main::config->{clientcert} ) ) {
            return undef unless ( -r "$main::config->{clientcert}" );
        }
        if ( exists( $main::config->{clientkey} ) ) {
            return undef unless ( -r "$main::config->{clientkey}" );
        }
        if ( exists( $main::config->{cacert} ) ) {
            return undef unless ( -r "$main::config->{cacert}" );
        }

        my $ldap;
        if ( lc($proto) eq "ldaps" ) {
            $ldap = Net::LDAPS->new(
                                    $hostname,
                                    port       => $port,
                                    version    => 3,
                                    verify     => 'require',
                                    clientcert => "$main::config->{clientcert}",
                                    clientkey  => "$main::config->{clientkey}",
                                    cafile     => "$main::config->{cacert}"
                                   );
        }
        elsif ( lc($proto) eq "ldap" ) {
            $ldap = Net::LDAP->new(
                                    $hostname,
                                    port    => $port,
                                    version => 3,
                                  );
        }

        if ($ldap) {
            my $mesg = $ldap->bind(
                                    "$main::config->{uid_attr}=$username,"
                                      . "$main::config->{base_passwd}",
                                    password => $password
                                  );

            $mesg->code() && die $mesg->error() . "\n";

            if ( $mesg->error() eq "Success" ) {

                # Determine if we're an admin:
                $mesg = $ldap->search(
                               base   => "$main::config->{base_passwd}",
                               filter => "($main::config->{uid_attr}=$username)"
                );
                $mesg->code() && die $mesg->error() . "\n";

                for my $entry ( $mesg->all_entries() ) {
                    my $isAdmin = $entry->get_value('LDAPadmin');
                    if ( $isAdmin && lc($isAdmin) eq "true" ) {
                        $main::userisAdmin = "true";
                    }
                    else {
                        $main::userisAdmin = "false";
                    }
                }
                $ldap->unbind();
                return 1;
            }
        }
        return undef;
    }

}

1;
