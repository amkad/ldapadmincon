#!/usr/local/bin/perl -w


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################


{

    package Utils;
    use strict;
    use Config;
    use Carp qw(carp confess cluck croak);
    use Fcntl;
    require Exporter;
    our @ISA       = ('Exporter');
    our @EXPORT_OK = qw(basename mktemp selfshift fgrep touch signo);

    sub new {    #Intentionally left un-prototyped
        my $proto = shift;

        my $class = ref($proto) || $proto;
        my $self;
        eval { $self = $class->SUPER::new(); };

        $self->{basename}  = \&basename;
        $self->{selfshift} = \&selfshift;
        $self->{mktemp}    = \&mktemp;
        $self->{touch}     = \&touch;
        $self->{fgrep}     = \&fgrep;
        $self->{signo}     = \&signo;

        bless( $self, $class );

        return $self;
    }

    sub selfshift(\$\@) {
        use vars qw(*self *args *package);
        *self = shift;
        *args = shift;
        my $package = caller;

        # support object-oriented version
        # $_[0] and $_[1] will be valid refs to %package::
        # and we only want 1 $self
        if ( ref( $args[0] ) eq *package{PACKAGE} ) {
            $self = shift @args;
        }
        if ( ref( $args[0] ) eq $package ) {

            # We have a real $self, don't munge it
            $self = shift @args;
        }
        else {

            # reference symbol table from calling package
            # since it won't be used in OOP style;
            #
            # This isn't a kludge; it's ok to relax
            # refs to manipulate the sysmbol table
            # if you're positive that's really what you
            # you intend to do.
            no strict 'refs';
            $self = \%{ $package . "::" };
            use strict 'refs';
            bless( $self, $package );
        }
    }

    sub basename($) {
        my $self;
        selfshift( $self, @_ );
        my $basename = substr( $_[0], rindex( $_[0], '/' ) );
        $basename =~ s/\///;
        return $basename;
    }

    sub mktemp(@) {
        my $self;
        selfshift( $self, @_ );
        my $filename = shift;

        my $randchar = sub {
            my @chars;
            for ( my $i = 48 ; $i < 122 ; $i++ ) {
                if (    ( $i >= 57 ) && ( $i <= 64 )
                     || ( $i >= 91 ) && ( $i <= 96 ) )
                {
                    next;
                }
                push @chars, pack( 'C', $i );
            }
            my $charnum = int( rand($#chars) );
            return $chars[$charnum];
        };

        my $tmp = ( $ENV{TMP} ) ? $ENV{TMP} : "/tmp";
        $filename = "$tmp/tmp.XXXXXXXXXXX" unless ($filename);
        $filename = "$tmp/$filename" unless ( $filename =~ m/\// );
        $filename =~ s/(X)(?=X|$)/&$randchar/eg;
        my $preserve_umask = umask(0077);
        touch($filename) || return undef;
        umask($preserve_umask);
        return $filename;
    }

    sub touch(@) {
        my $self;
        selfshift( $self, @_ );
        my $filename = shift;
        my $atime    = shift;
        my $mtime    = shift;

        $filename =~ m/(.*)/;
        $filename = $1;

        unless ( sysopen( *HANDLE, $filename, Fcntl::O_CREAT() ) ) {
            carp "Can't touch $filename $!\n";
            return undef;
        }
        close(HANDLE);
        utime( $atime || undef, $mtime || undef, $filename );
    }

    sub signo(@) {
        my $self;
        selfshift( $self, @_ );
        my $sig_name = shift;
        my $i        = 0;
        my %signo;

        defined $Config{sig_name} || die "No sigs?" . "\n";
        foreach ( split( ' ', $Config{sig_name} ) ) {
            $signo{$_} = $i++;
        }
        return $signo{$sig_name};
    }

    sub fgrep(@) {
        my $self;
        selfshift( $self, @_ );
        my $regexp = shift;
        my $file   = shift;
        my @ret;

        $file =~ m/(.*)/;
        $file = $1;

        $regexp =~ s/(?:^\/|\/$)//g;

        if ( ref($file) eq "GLOB" ) {
            seek( $file, 0, Fcntl::SEEK_SET() );
            @ret = grep( /$regexp/, <$file> ) || return undef;
            seek( $file, 0, Fcntl::SEEK_SET() );
        }
        else {
            open( FILE, "< $file" )
              || ( carp("Unable to open $file $!")
                   && return undef );
            @ret = grep( /$regexp/, <FILE> );
            close(FILE);
        }
        return @ret;
    }
    1;
}

