#!/usr/local/bin/perl -w


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################



require "config.ph";

{

    package LDAPtools;
    use Net::LDAP;
    use Net::LDAPS;
    use Exporter;

    our @ISA       = qw(Exporter);
    our @EXPORT_OK = qw(parseURI makeDN UpdateAttr BindLDAP);

    sub parseURI(@) {
        my $URI = shift;
        $URI =~ m!(ldaps?)://(.*?):(\d{1,7})/?!;
        return ( $1, $2, $3 );
    }

    sub makeDN(@) {
        my $username = shift;
        return "uid=$username," . $main::config->{base_passwd};
    }

    sub UpdateAttr(@) {
        my $dn                = shift;
        my $ldap              = shift;
        my %search_opts       = %{ +shift };
        my %attributes_values = %{ +shift };
        my %oldvalues;
        my @status;

        $mesg = $ldap->search(%search_opts);

        for $entry ( $mesg->all_entries() ) {
            for ( keys %attributes_values ) {
                ## Might as well support deleting values in this method, since we do everything else
                if ( ref( $attributes_values{$_} ) eq "ARRAY" ) {
                    if ( @{ $attributes_values{$_} }[0] ) {
                        $oldvalues{$_}         = @{ $attributes_values{$_} }[0];
                        $attributes_values{$_} = undef;
                    }
                    else {
                        $oldvalues{$_}         = $entry->get_value($_);
                        $attributes_values{$_} = undef;
                    }
                }
                else {
                    $oldvalues{$_} = $entry->get_value($_);
                }
            }
        }

        $mesg->code() && die "BindLDAP: " . $mesg->error() . "\n";

        for ( keys %attributes_values ) {
            if (    defined( $attributes_values{$_} )
                 && defined( $oldvalues{$_} ) )
            {
                $mesg = $ldap->modify( $dn,
                                  replace => { $_ => $attributes_values{$_} } );
            }
            elsif ( defined( $attributes_values{$_} ) ) {
                $mesg =
                  $ldap->modify( $dn, add => { $_ => $attributes_values{$_} } );
            }
            elsif ( defined( $oldvalues{$_} )
                    && !defined( $attributes_values{$_} ) )
            {
                $mesg =
                  $ldap->modify( $dn, delete => { $_ => [ $oldvalues{$_} ] } );
            }

            $mesg->code() && die "BindLDAP: " . $mesg->error() . "\n";
        }
        $mesg->code() && die "BindLDAP: " . $mesg->error() . "\n";
        return 1;
    }

    sub BindLDAP(@) {
        my $rootbinddn = $main::config->{adminbinddn};
        my $rootbindpw = $main::config->{adminbindpw};

        my ( $proto, $hostname, $port ) = parseURI("$main::config->{uri}");

        return undef unless ( $rootbindpw && $rootbinddn );
        if ( exists( $main::config->{clientcert} ) ) {
            die "clientcert not found"
              unless ( -r "$main::config->{clientcert}" ) . "\n";
        }
        if ( exists( $main::config->{clientkey} ) ) {
            die "clientkey not found"
              unless ( -r "$main::config->{clientkey}" ) . "\n";
        }
        if ( exists( $main::config->{cacert} ) ) {
            die "cacert not found"
              unless ( -r "$main::config->{cacert}" ) . "\n";
        }

        my $ldap;
        if ( lc($proto) eq "ldaps" ) {
            $ldap = Net::LDAPS->new(
                                    $hostname,
                                    port       => $port,
                                    version    => 3,
                                    verify     => 'require',
                                    clientcert => "$main::config->{clientcert}",
                                    clientkey  => "$main::config->{clientkey}",
                                    cafile     => "$main::config->{cacert}"
                                   );
        }
        elsif ( lc($proto) eq "ldap" ) {
            $ldap = Net::LDAP->new(
                                    $hostname,
                                    port    => $port,
                                    version => 3,
                                  );
        }

        if ($ldap) {
            my $mesg = $ldap->bind( "$rootbinddn", password => $rootbindpw );

            $mesg->code() && die "BindLDAP: " . $mesg->error() . "\n";
            return $ldap;
        }
        return undef;
    }
}

