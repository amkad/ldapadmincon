#!/usr/local/bin/perl -w


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################


use strict;

{

    package Templates;
    require Exporter;

    #use CGI qw(:all);

    our @ISA       = qw(Exporter);
    our @EXPORT_OK = qw(tpl);

    sub tpl(@) {
        my $file   = shift;
        my $tpldir = $main::config->{install_dir} . "/Screens/templates";

        local $SIG{__WARN__} = sub {
            chomp( my $message = $_[0] );
            print STDERR "$message in file $file\n";
        };

        my $outfile;
        if ( -f "$tpldir/$file" ) {
            open( OUTFILE, ">", \$outfile );
            open( INFILE, "< $tpldir/$file" )
              || do { print "can't open templatefile\n"; return undef; };

            my $evalstring;
            while (<INFILE>) {
                m/\[\[(.*?)\]\]/ && do {
                    if ( $1 && $_ ) {
                        s/\[\[(.*?)\]\]/eval "$1";/gxe;
                    }
                };
                /(.*?)\[\[(.*?)/ && do {
                    print OUTFILE $1;
                    $evalstring = $2 || "#\n";
                    next;
                };
                if ($evalstring) {
                    /(.*?)\]\](.*?)/ && do {
                        $evalstring .= $1 || "\n#";
                        print OUTFILE eval "$evalstring";
                        print OUTFILE $2;
                        $evalstring = undef;
                        next;
                    };
                    $evalstring .= $_;
                    next;
                }
                print OUTFILE $@;
                print OUTFILE;
            }
        }
        return $outfile;
    }
}
1;

