#!/usr/local/bin/perl -w


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################



package Modules;
use strict;
use Utils qw(selfshift);

require Exporter;
our @ISA       = ("Exporter");
our @EXPORT_OK = qw(Load Use);

sub new {    #Intentionally left un-prototyped
    my $proto = shift;

    my $class = ref($proto) || $proto;
    my $self;
    eval { $self = $class->SUPER::new(); };

    $self->{Use}  = \&Use;
    $self->{Load} = \&Load;

    bless( $self, $class );

    return $self;
}

sub Use(@) {
    my $self;
    selfshift( $self, @_ );
    my $module   = shift;
    my $filename = $module;
    $filename =~ s/\:\:/\//g;
    my $symtable;
    eval {
        no strict 'vars';
        $symtable = ref( \%{ $module . "::" } );
        use strict 'vars';
    };
    eval {
        require $filename . ".pm" unless ( defined($symtable) );
        import $module @_;
    };
    return 1;
}

sub Load(@) {
    my $self;
    selfshift( $self, @_ );
    my $module = shift;
    return $module->new(@_) if ( $self->{Use}($module) );
}

=head1 NAME

Modules

=head1 SYNOPSIS

 use Modules qw(Load Use);

 Use qw(SomeModule);
 Use qw(SomeOther::Package::Module::thing);

 OR: 

 my $Handle = Load qw(SomeModule);

 Don't use both Use and Load; it's unnecessary, and probably 
 a bad idea;

 If you prefer not to mangle your symbol table, You can also 
 use this in OOP style like this:

 use Modules;

 my $handle=Modules->new();
 my $SomewhereHandle = $handle->Load("Some::Modules::Somewhere");


=head1 DESCRIPTION

=head2 Use: 

 In Perl, the 'use' constructor is basically a macro for 
 BEGIN { require Module; import Module };
 where "Module" is a bareword.
 
 This Librarly provides a "Use" constructor that is that 
 exact macro EXCEPT that it does not reside in a BEGIN
 block; and allowing for the easy inclusion of Modules at 
 run-time; using variable names for modules instead of 
 barewords. In essence; on-demand module loading.

=head2 Load:

 Load performs the function of Use, but also returns
 the blessed handle from the constructor "new";
 This of course only works in the module being loaded
 contains a function called "new" that is designated to 
 be the constructor for the package. 
 This is a convenience function only; YMMV
 
 Use and Load are both available for Export; but neither
 are exported by default.

=cut

1;

