#!/usr/local/bin/perl -w


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################


use strict;
{

    package LDWSession;
    use CGI qw(:utf8);
    use Utils qw(touch);
    use Exporter;

    require "config.ph";

    our @ISA       = qw/Exporter/;
    our @EXPORT_OK =
      qw/valid fetch_old_state save_state create_session create_ssid encode_ssid/;

    use vars '*cgi';

    sub new {
        my $proto = shift;

        my $class = ref($proto) || $proto;
        my $self;
        eval { $self = $class->SUPER::new(); };

        $self->{key} = undef;

        bless( $self, $class );

        *cgi = \$main::cgi;

        return $self;
    }

    sub valid(@) {
        my $self      = shift;
        my $key       = shift;
        my $ssid      = shift;
        my $valid_ses = "";

        return undef unless ( $key && $ssid );

        # Clean up all old sessions
        opendir( STATEDIR, "$main::config->{STATE_DIR}" );
        for my $session ( readdir(STATEDIR) ) {
            $session =~ m/(.*)/;
            $session = $1;
            my (
                 $dev,   $ino,     $mode, $nlink, $uid,
                 $gid,   $rdev,    $size, $atime, $mtime,
                 $ctime, $blksize, $blocks
               )
              = stat("$main::config->{STATE_DIR}/$session");

            if ( time() - 900 > $mtime ) {
                if ( $session eq $key ) {
                    $main::expired = 1;
                }
                unlink("$main::config->{STATE_DIR}/$session");
            }
        }

        if ( $key =~ /^[0-9a-zA-Z]{20}$/ ) {
            if ( -f "$main::config->{STATE_DIR}/$key" ) {
                open( SESSION, "$main::config->{STATE_DIR}/$key" );
                while (<SESSION>) {
                    chomp;
                    my ( $skey, $sval ) = split( '=', $_, 2 );
                    for ($skey) {

                     # Double check the datestamp we added, just in case somehow
                     # mtimes aren't reliable
                        /^sessiontime$/ && do {
                            if ( time() < $sval + 900 ) {
                                $valid_ses = "true"
                                  unless ( $valid_ses eq "false" );
                            }
                            else {
                                $valid_ses = "false";
                            }
                        };
                        /^ssid$/ && do {
                            my $str       = $self->encode_ssid($ssid);
                            my $comp_ssid = $self->encode_ssid($sval);

                            if ( $str eq $comp_ssid ) {
                                $valid_ses = "true"
                                  unless ( $valid_ses eq "false" );
                            }
                            else {
                                $valid_ses = "false";
                            }
                        };
                    }
                }
                close(SESSION);
                return 1 if ( $valid_ses eq "true" );
            }
        }
        return undef;
    }

    sub fetch_old_state(@) {
        my $self        = shift;
        my $session_key = shift;

        $session_key =~ /^([0-9a-zA-Z]{20})$/;
        $session_key = $1;

        open( SAVEDSTATE, "< $main::config->{STATE_DIR}/$session_key" );
        $cgi = new CGI( \*SAVEDSTATE );
        $cgi->charset('UTF-8');
        close SAVEDSTATE;
    }

    sub save_state(@) {
        my $self        = shift;
        my $session_key = shift;

        $session_key =~ /^([0-9a-zA-Z]{20})$/;
        $session_key = $1;

        touch("$main::config->{STATE_DIR}/$session_key");
        open( SAVEDSTATE, "> $main::config->{STATE_DIR}/$session_key" )
          || die "Failed opening session state file: $!" . "\n";
        $cgi->save( \*SAVEDSTATE );
        close SAVEDSTATE;
    }

    sub create_ssid(@) {
        my $self = shift;
        my $ssid_key;
        for ( my $i = 0 ; $i < 20 ; $i++ ) { $ssid_key .= 'X'; }

        my $randchar = sub {
            my @chars;
            for ( my $i = 1 ; $i < 255 ; $i++ ) {
                push @chars, pack( 'C', $i );
            }
            my $charnum = int( rand($#chars) );
            return $chars[$charnum];
        };

        $ssid_key =~ s/(X)(?=X|$)/&$randchar/eg;

        $ssid_key =~ s/([^A-Za-z0-9])/sprintf("%%%02X", ord($1))/seg;
        return $ssid_key;
    }

    sub create_session(@) {
        my $self = shift;
        my $session_key;
        for ( my $i = 0 ; $i < 20 ; $i++ ) { $session_key .= 'X'; }

        my $randchar = sub {
            my @chars;
            for ( my $i = 48 ; $i < 122 ; $i++ ) {
                if (    ( $i >= 57 ) && ( $i <= 64 )
                     || ( $i >= 91 ) && ( $i <= 96 ) )
                {
                    next;
                }
                push @chars, pack( 'C', $i );
            }
            my $charnum = int( rand($#chars) );
            return $chars[$charnum];
        };

        $session_key =~ s/(X)(?=X|$)/&$randchar/eg;

        return $session_key;
    }

    sub encode_ssid(@) {
        my $self = shift;
        my $ssid = shift;
        my $str  = $self->decode_ssid($ssid);
        $str =~ s/([^A-Za-z0-9])/sprintf("%%%02X", ord($1))/seg;

        return $str;
    }

    sub decode_ssid(@) {
        my $self = shift;
        my $ssid = shift;
        my $str  = $ssid;

        while ( $str =~ m/(?:\\x([A-Fa-f0-9]{2}).*){3,20}/ ) {
            $str =~ s/\\x([A-Fa-f0-9]{2})/pack('C', hex($1))/seg;
        }

        while ( $str =~ m/(?:\%[0-9A-Fa-f]{2}.*?){3,20}/ ) {
            $str =~ s/\%([A-Fa-f0-9]{2})/pack('C', hex($1))/seg;
        }

        return $str;
    }
}
1;
