#!/usr/bin/perl -w


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################


use strict;
use Net::SMTP;

{

    package Email;
    our $SMTP_SERVER;
    our $THIS_HOST;

    sub new {
        my $proto = shift;
        my %args  = @_;

        my $class = ref($proto) || $proto;
        my $self;
        eval { $self = $class->SUPER::new(); };

        $self->{FormatTo}    = \&FormatTo;
        $self->{SendMessage} = \&SendMessage;

        $SMTP_SERVER = $args{Server} if ( defined $args{Server} );
        $THIS_HOST   = $args{Host}   if ( defined $args{Host} );

        bless( $self, $class );
        return $self;
    }

    sub FormatTo(@) {
        my $self     = shift;
        my @toList   = @_;
        my $tostring = "";
        if ( scalar @toList > 1 ) {
            for ( my $i = 0 ; $i < scalar @toList ; $i++ ) {
                $tostring .= $toList[$i] . ", ";
            }
        }
        return $tostring;
    }

    sub SendMessage(@) {
        my $self   = shift;
        my %Values = @_;

        my $subject = $Values{subject} if ( defined $Values{subject} );
        my $from    = $Values{from}    if ( defined $Values{from} );
        my @message = @{ $Values{message} };

        my @toList;
        my @ccList;
        my @bccList;

        my $smtp = Net::SMTP->new(
                                   $SMTP_SERVER,
                                   Hello   => $THIS_HOST,
                                   Timeout => 30,
                                   Debug   => 0
                                 );
        return undef if ( !defined($smtp) );

        if ( defined $Values{to} ) {
            if ( ref( $Values{to} ) ) {
                @toList = @{ $Values{to} };
            }
            else {
                @toList = ( $Values{to} );
            }
        }
        if ( defined $Values{cc} ) {
            if ( ref( $Values{cc} ) ) {
                @ccList = @{ $Values{cc} };
            }
            else {
                @ccList = ( $Values{cc} );
            }
        }
        if ( defined $Values{bcc} ) {
            if ( ref( $Values{bcc} ) ) {
                @bccList = @{ $Values{bcc} };
            }
            else {
                @bccList = ( $Values{cc} );
            }
        }

        # These methods do NOT affect the e-mail header
        $smtp->mail($from);

        # Conjoin all recipients together to send ther server RCPT's
        for my $to ( @toList, @ccList, @bccList ) {
            $smtp->to($to);
        }

        # begin data segment (the message)
        $smtp->data();

        # Format and send header
        $smtp->datasend("From: $from\n");
        $smtp->datasend("Subject: $subject\n");
        $smtp->datasend( "To: " . $self->FormatTo(@toList) . "\n" );
        $smtp->datasend( "Cc: " . $self->FormatTo(@ccList) . "\n" );
        $smtp->datasend("\n");

        # Format message so that all lines have a newline terminator.
        # Chomps first to ensure no double newlines happen.
        my $UserName = $toList[0];
        $UserName =~ s/\@.*//;
        for (@message) {
            chomp;
            $smtp->datasend( $_ . "\n" );
        }

        # We're done
        $smtp->dataend();
        return 1 if ( $smtp->quit() );
        return undef;
    }

    # End package

=head1 NAME


Email


=head1 SYNOPSIS


 my @Message=("This is a test of the e-mail system","This is only a test");
 my @To=('user1@someserver.com','user2@someotherserver.com');
 my @Cc=('user3@yetanotherserver.com','user4@server15.com');
 my @Bcc=('blah@blahblah.com','thud@thud.com','crap@crap.com');
 my $subject="This is a test message";
                                                                                                                             
 my $email = Email->new(Server=>'smtp-server.mydomain.net',Host=>'fromthishost.mydomain.net');
                                                                                                                             
 $email->SendMessage(	subject=>$subject,to => \@To,
			from=>'someuser@someserver.com',
			cc=>\@Cc,
			bcc=>\@Bcc,
                        message=>\@Message
                   );

                                                                                                                             
=head1 DESCRIPTION


  Class provides an easy interface to send e-mails via Net::SMTP.
  
  Constructor: new (	Server=>'servername.serverdomain.net',
			Host=>'thishostname.thisdomain.net');

  	Server: Name of SMTP server you're sending this through.
	Host: Name of the host I'm on to send the SMTP server via EHLO (or HELO)

  Methods:
  
  SendMessage( 	subject =>, 
		from =>,
		to=>,
		cc=>,
		bcc=>,
		message=>
	     )

	subject: Subject line for e-mail. (required)
	from: Sender (required)
	to: Recipient (scalar) or recpients (list reference [i.e. \@])
	cc: Same as to, but for cc
	bcc: Same as to, but for bcc
	message: List reference to e-mail body (i.e. my @message=<MESSAGE>)

=head1 BUGS

  Has em, look for em, fix em.

=head1 AUTHOR

  Jamie Beverly <jbeverly1@tampabay.rr.com>

=cut

}
1;

