#!/usr/local/bin/perl -w


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################


BEGIN {
    require "config.ph";
}

use CGI qw(:utf8);
use LDWSession;
use Screens;
use Modules qw(Use Load);
use Auth;

use vars '*params *constant_params *cgi';
*cgi = \$main::cgi;

my $ses = LDWSession->new();
$cgi = CGI->new();
$cgi->charset('UTF-8');

*params = \$main::params;

sub vars() {
    my $subparams;
    foreach my $param ( $cgi->param() ) {
        my @values = $cgi->param($param);

        if ( scalar(@values) > 1 ) {
            push( @{ $subparams->{$param} }, $cgi->param($param) );
        }
        else {
            $subparams->{$param} = $cgi->param($param);
        }
    }
    return $subparams;
}

our $constant_params;

$params = vars();

if ( $ENV{QUERY_STRING} ) {
    my @args = split( '&', $ENV{QUERY_STRING} );
    for (@args) {
        chomp;
        my ( $key, $val ) = split('=');
        $key =~ /authusername/ && next;
        $key =~ /password/     && next;
        $params->{$key} = $val;
    }
}

$session_key = $params->{session_id} || "";
$ssid        = $params->{ssid}       || "";

if ( $params->{action} && $params->{action} eq "logout" ) {
    if ( $ses->valid( $session_key, $ssid ) ) {
        if ( $params->{session_id} =~ m/([A-Za-z0-9]{20})/ ) {
            unlink("$main::config->{STATE_DIR}/$1");
        }
    }
    print $cgi->redirect('index.cgi');
    print $cgi->header();
    exit(0);
}

unless ( $ses->valid( $session_key, $ssid ) ) {
    eval {
        if ( $params->{authusername} && $params->{password} )
        {
            if (
                 Auth::Authentication(
                                       $params->{authusername},
                                       $params->{password}
                                     )
               )
            {
                $session_key = $ses->create_session();
                $ssid        = $ses->create_ssid();
                if ( $main::userisAdmin eq "true" ) {
                    $cgi->param( -name => 'userisAdmin', -value => 'true' );
                }
                $cgi->param( -name => 'password',    -value => '--removed--' );
                $cgi->param( -name => 'ssid',        -value => $ssid );
                $cgi->param( -name => 'sessiontime', -value => time() );
                $cgi->param( -name => 'state',       -value => undef );
                $ses->save_state($session_key);
                print $cgi->redirect(
                          $cgi->url() . "?session_id=$session_key&ssid=$ssid" );
                print $cgi->header();
                exit(0);
            }
            else {
                $main::invalid_pw = 1;
                print $cgi->header();
                print Screens::AuthScreen();
                exit(0);
            }
        }
        else {
            print $cgi->header();
            print Screens::AuthScreen();
            exit(0);
        }
    };
    print $@;
}

$ses->fetch_old_state($session_key);
$constant_params = vars();

$cgi->param( -name => 'sessiontime', -value => time() );
for ( keys %{$constant_params} ) {
    /screen/ && next;
    if ( $params->{userisAdmin} && $params->{userisAdmin} eq "false" ) {
        /userisAdmin/ && do {
            $cgi->param( -name => 'userisAdmin', -value => 'false' );
            next;
        };
    }
    $cgi->param( -name => $_, -value => $params->{$_} );
    $params->{$_} = $constant_params->{$_};
}
$params->{ssid} = $ses->encode_ssid( $params->{ssid} ) if ( $params->{ssid} );
$ses->save_state($session_key);

print $cgi->header();
1;
