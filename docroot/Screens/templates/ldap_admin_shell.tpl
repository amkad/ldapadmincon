<link href="ldapadmin.css" rel="stylesheet" type="text/css">

<body onLoad="swapLayers('[[ $main::default_layer ]]');"> 

<div id="Layer1" style="position:absolute; width:744px; height:91px; z-index:3; left: 0px; top: 0px;">
	<img src="Images/t_bar_full.gif" width="744" height="85">
</div>


<div id="navmenutop" style="position:absolute; left:5px; top:105px; width:109px; height:379px; z-index:3; background-color: #F8F8F9; layer-background-color: #F8F8F9; background-image: url(Images/bg_grid.gif)" class="nav_box">
  	<div id="LDAPtitle" style="position:absolute; width:120px; height:65px; z-index:11; left: 8px; top: 5px;">
 	 	<p style="font-weight: 900; font-size:16px; color: #000000">LDAP Administration Console<br>
		<a href="index.cgi?session_id=[[ $main::params->{session_id} if(defined($main::params->{session_id})); ]]&ssid=[[ $main::params->{ssid} if(defined($main::params->{ssid})); ]]&action=logout" class="nav_item_small">Logout</a>
		</p>
	</div>
  	<p>&nbsp; </p>
  	<p>&nbsp; </p>
  	<hr>
	[[  
		my @items;
		for(keys %main::plugins) {
			my $string = '<p class="nav_items"><a href="index.cgi?session_id=' . 
				$main::params->{session_id} . 
				'&ssid=' . $main::params->{ssid} .
				'&screen=' . 
				$_ . 
				'" class="nav_items">'. $_ . '</a></p><hr>';
			if(exists($main::plugins{$_}->{index})) {
				my $tempstring;
				if($items[$main::plugins{$_}->{index}]) {
					$tempstring = $items[$main::plugins{$_}->{index}];
				}
				$items[$main::plugins{$_}->{index}] = $string;
				if($tempstring) {
					push @items, $tempstring;
				}
			} else {
				push @items, $string;
			}
		}
		my $string = join("\n",@items);  
	]]

	[[
		my $string='';
		if($main::params->{userisAdmin} && $main::params->{userisAdmin} eq "true") {
			$string .= '<div id="switchtouser" style="position:absolute; top: 389px; ' .
			'left: 18px; width: 120px; height: 20px;">' . "\n" .
			'<a class="nav_item_small" href="index.cgi?session_id=' . 
			$main::params->{session_id} . 
			'&ssid=' . $main::params->{ssid} . 
			'&userisAdmin=false">Switch to User view</a>' . "\n" .
			'</div>';
		}
		$string;
	]]
</div>


<div id="navmenudshadow" style="position:absolute; left:10px; top:110px; width:145px; height:415px; z-index:1; background-color: #999999; layer-background-color: #999999;">
</div>


<div id="navmenutop" style="position:absolute; left:160px; top:105px; width:539px; height:379px; z-index:25; background-color: #F8F8F9; layer-background-color: #F8F8F9; background-image: url(Images/bg_grid.gif)" class="nav_box">
</div>
<div id="navmenudshadow" style="position:absolute; left:165px; top:110px; width:575px; height:415px; z-index:1; background-color: #999999; layer-background-color: #999999;">
</div> 


