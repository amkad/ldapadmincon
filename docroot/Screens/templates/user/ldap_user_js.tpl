<script language="Javascript">
<!-- 

var LDAPusers = new Object;

[[ join('',@main::userlist);  ]]

function populateForm(input) {
	var username = input;
	var divs = new Array;
	divs.UserInformation = true;
	for(obj in divs) {
		if(LDAPusers[username] == null) {
			if(obj == "AddUserInformation") {
				document[obj].username.value="";
			} else {
				document[obj].username.value = "-----------------------";
			}
			document[obj].email.value = "";
			document[obj].firstname.value = "";
			document[obj].lastname.value = "";
			document[obj].ldapadmin.checked = false;
			continue;
		}
	
		document[obj].username.value = username;
		document[obj].email.value = LDAPusers[username].email;
		document[obj].firstname.value = LDAPusers[username].firstname;
		document[obj].lastname.value = LDAPusers[username].lastname;
	
		if(LDAPusers[username].LDAPadmin) {
			if(LDAPusers[username].LDAPadmin == "true") {
				document[obj].ldapadmin.checked = true;
			} else {
				document[obj].ldapadmin.checked = false;
			}
		}
        if(LDAPusers[username].locked) {
			var locked_elem = getElemRefs('locked');
			locked_elem.childNodes.item(0).data = "Account Locked";
			locked_elem.style.color = "#FF1515";
		} else {
			var locked_elem = getElemRefs('locked');
			locked_elem.childNodes.item(0).data = '';
		}
	}
}

function reportError(errorString) {
	var Table = getElemRefs('errormessage_table');

	var newTR = document.createElement('tr');
	var newTD = document.createElement('td');
	newTD.style.textAlign='center';
	newTD.style.color = '#FF1515';
	newTD.style.fontSize = '11px';
	newTR.appendChild(newTD);
	var newText = document.createTextNode(errorString);
	newTD.appendChild(newText);

	Table.appendChild(newTR);
}

function validPassword(form, inString) {
	var minLength   = [[ $main::config->{pass_strength}->{MinLength}; ]];
	var minUpper    = [[ $main::config->{pass_strength}->{RequireUpper}; ]];
	var minLower    = [[ $main::config->{pass_strength}->{RequireLower}; ]];
	var minDigits   = [[ $main::config->{pass_strength}->{RequireDigits}; ]];
	var minSpecial  = [[ $main::config->{pass_strength}->{RequireSpecial}; ]];
	var retval = true;

	var specialChars = '\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\[\\]\\{\\}\\.\\,\\<\\>\\?\\/\\\\\|\\~\\`\\:\\;\\\'\\\"';
	
	var Table = getElemRefs('errormessage_table');
	while(Table.hasChildNodes()) {
		Table.removeChild(Table.lastChild);
	}

	if(inString.length < minLength) {
		reportError("Password is shorter than " + minLength + " charectars");
	}

	var num = new Object;
	num.upper = 0;
	num.lower = 0;
	num.digits = 0;
	num.special = 0;
	
	for(i=0;i<inString.length;i++) {
		var testChar = inString.substr(i,1);
		var re = new RegExp;

		re.compile('[A-Z]');
		if(re.test(testChar)) {
			num.upper++;
		}
		
		re.compile('[a-z]');
		if(re.test(testChar)) {
			num.lower++;
		}
		
		re.compile('[0-9]');
		if(re.test(testChar)) {
			num.digits++;
		}
		
		re.compile('[' + specialChars + ']');
		if(re.test(testChar)) {
			num.special++;
		}
	}

	if(num.upper < minUpper) {
		reportError("Password contains less than " + minUpper + " upper case charectars");
		retval = false;
	}
	if(num.lower < minLower) {
		reportError("Password contains less than " + minLower + " lower case charectars");
		retval = false;
	}
	if(num.digits < minDigits) {
		reportError("Password contains less than " + minDigits + " digit charectars");
		retval = false;
	}
	if(num.special < minSpecial) {
		reportError("Password contains less than " + minSpecial + " special charectars");
		retval = false;
	}
	return retval;
}


function matchesFirst(form, inString, field1) {
	var otherpassword = form[field1].value;

	if(inString != otherpassword) {
		form.newpassword2.style.backgroundColor = "#ff1515";
		return;
	}
	form.newpassword2.style.backgroundColor = null;
	return true;
}

function validPasswordForm(input) {
	var form = input;
	if(validPassword(form, form.newpassword1.value) && matchesFirst(form, form.newpassword2.value, 'newpassword2')) {
		return true;
	} else {
		form.newpassword1.focus();
		form.onSubmit = false;
		form.Submit = false;
		return false;
	}
}
	
//-->
</script>
