<div id="users_sub_nav_menu" style="position:absolute; width:565px; height:45px; z-index:200; left: 165px; top: 110px; visibility: visible;">
		<table width="50%" border="0" cellspacing="0" cellpadding="4" align="left">
			<tr align="center">
				<td><a href="#" class="nav_item_small" onClick="swapLayers('user_information');">User Information</a></td>
				<td>|</td>
				<td><a href="#" class="nav_item_small" onClick="swapLayers('change_password')">Change Password</a></td>
				<td>|</td>
<!--				<td><a href="#" class="nav_item_small" onClick="swapLayers('modusers');">Something tbd</a></td>
				<td>|</td>
				<td><a href="#" class="nav_item_small" onClick="swapLayers('lockusers');">Lock User</a></td>
				<td>|</td>
				<td><a href="#" class="nav_item_small" onClick="swapLayers('passreset');">Password Resets</a></td> -->
			</tr>
		</table><br>
		<hr>
	</center>
</div>

<!-- ###################### User Information layer ###################### -->

<div id="user_information" style="visibility: hidden; position:absolute; width:565px; height:385px; z-index:201; left: 165px; top: 130px;">
    <div style="border: 2px ridge #000099; padding: 0px; margin-top: 0px; visibility: inherit; position: absolute; width: 50px; height: 1px; top: 0; left: 33px; vertical-align: top; font-size: 0px;">		
	</div>
	<form name="UserInformation">
  	<div style="visibility: inherit; position: absolute; width: 565px; height: 365px; top: 12px; left: 0px; z-index: 202">
		<div style="visibility: inherit; position: absolute; width: 275px; height: 340px; top: 10px; left: 10px; z-index: 1000; border: 3px groove #9999FF">
			<table width="100%">
			<tr>
				<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
					Username:  
				</td>
				<td colspan="2">
					<input name="username" style="font-size: 9px; background-color: #cfd0d8" type="text" size="15" readonly="true">
				</td>
			</tr>
			<tr>
				<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
					First name:
				</td>
				<td colspan="2">
					<input name="firstname" style="font-size: 9px; background-color:#CFD0D8" type="text" size="15" readonly="true">
				</td>
			</tr>
			<tr>
				<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
					Last name:
				</td>
				<td colspan="2">
					<input name="lastname" style="font-size: 9px; background-color:#CFD0D8" type="text" size="15" readonly="true">
				</td>
			</tr>
			<tr>
			<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
				E-mail Address:
			</td>
			<td colspan="2">
				<input name="email" style="font-size: 9px; background-color:#CFD0D8" type="text" size="25" readonly="true">
			</td>
		</tr>
	</table>
	</div>
		<div style="visibility: inherit; position: absolute; width: 245px; height: 130px; top: 10px; left: 305px; z-index: 1000; border: 3px groove #9999FF">
  			<table width="100%">
				<tr>
					<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
						<hr style="font-size: 11px; font-weight: 900; color: #000099; background-color: #000099">
					</td>
				</tr>
				<tr>
					<td style="font-size: 11px; font-weight: 900; color: #000099;">
						User is an LDAP Console Administrator?
					</td>
					<td style="font-size: 11px; font-weight: 900; color: #000099;">
						<input name='ldapadmin' type="checkbox" disabled>
					</td>
				</tr>
				<tr>
					<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
						<hr style="font-size: 11px; font-weight: 900; color: #000099; background-color: #000099">
					</td>
				</tr>
				<tr>
					<td id="locked" style="font-size: 11px; font-weight: 900; color: #FF1515;">
						&nbsp;
					</td>
				</tr>
			</table>
		</div>		
  	</div>
	</form>
</div>




<!-- ###################### Change password layer ###################### -->


<div id="change_password" style="visibility: hidden; position:absolute; width:565px; height:385px; z-index:201; left: 165px; top: 130px;">
    <div style="border: 2px ridge #000099; padding: 0px; margin-top: 0px; visibility: inherit; position: absolute; width: 50px; height: 1px; top: 0; left: 175px; vertical-align: top; font-size: 0px;">		
	</div>
	<form onSubmit="return validPasswordForm(this);" name="changepassword" method="post" action="index.cgi?session_id=[[ $main::params->{session_id} ]]&ssid=[[ $main::params->{ssid} ]]&screen=[[ $main::params->{screen} ]]&action=changepassword">
  	<div style="visibility: inherit; position: absolute; width: 565px; height: 365px; top: 12px; left: 0px; z-index: 202">
		<div style="visibility: inherit; position: absolute; width: 275px; height: 160px; top: 65px; left: 145px; z-index: 1000; border: 3px groove #9999FF; text-align: center;">
			&nbsp;<br>
			<table width="100%" border="0px" cellpadding="2px" cellspacing="5px">
				<tr>
					<td style="font-size: 11px; color: #000099; font-weight: 300" align="right">
						New Password: 
					</td>
					<td> 
						<input name="newpassword1" type="password" size="20" style="font-size: 9px" onchange="validPassword(document.changepassword, this.value)">
					</td>
				</tr>
				<tr>
					<td style="font-size: 11px; color: #000099; font-weight: 300" align="right"> 
						New Password again: 
					</td>
					<td> 
						<input name="newpassword2" type="password" size="20" style="font-size: 9px" onKeyUp="matchesFirst(document.changepassword, this.value, 'newpassword1')">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<hr>
					</td>
				</tr>
				<tr>
					<td>
						<button onmouseup="this.style.borderStyle = 'outset';" onmousedown="this.style.borderStyle = 'inset';" type="submit" style="border-style: outset; border-width: 3px; border-color: #6666CC">Submit</button>
					</td>
					<td>
						<button onmouseup="this.style.borderStyle = 'outset';" onmousedown="this.style.borderStyle = 'inset';" type="reset" style="border-style: outset; border-width: 3px; border-color: #6666CC">Clear</button>
					</td>
				</tr>
			</table>
		</div>
		</form>
		<div id="errormessages" style="position:absolute; top: 235px; left: 145px; height: 100px; width: 275px">
			<table align="center" style="text-align: center" id="errormessage_table" width="100%" border="0" cellpadding="0" cellspacing="0">
			</table>
		</div>
  	</div>
</div>
</body>
</html>
