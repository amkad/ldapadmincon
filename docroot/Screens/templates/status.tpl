<div id="status_screen" style="visibility: visible; position:absolute; 
	width:565px; height:385px; z-index:201; left: 165px; top: 130px;">
	<h2>Status:</h2>
	<table border="0" width="100%">
	
[[ 
my @retval;
my $action = $main::action_string || "";
if(@main::errors && scalar @main::errors > 0) {
	for(@main::errors) {
		push @retval, '<tr>';
		push @retval, '<td style="color: #ff1515; font-size: 16px">';
		push @retval, "Error: $_";
		push @retval, '</td></tr>';
	}
}
if(@main::warnings && scalar @main::warnings > 0) {
	for(@main::warnings) {
		push @retval, '<tr>';
		push @retval, '<td style="color: #ff8515; font-size: 16px">';
		push @retval, "Warning: $_";
		push @retval, '</td></tr>';
	}
}

if(@main::messages && scalar @main::messages > 0) {
	for(@main::messages) {
		push @retval, '<tr>';
		push @retval, '<td style="color: #000099; font-size: 18px">';
		push @retval, "$_";
		push @retval, '</td></tr>';
	}
}

if( (!@main::errors) || (scalar @main::errors < 1) ) {
	push @retval, '<tr>';
	push @retval, '<td style="color: #000099; font-size: 18px">';
	push @retval, 'Operation successful: ' . $action;
	push @retval, '</td></tr>';
}
join('',@retval);
]]
	</table>
    </div>
</body>
</html>
				
