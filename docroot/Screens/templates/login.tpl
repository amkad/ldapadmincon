
<html>
<head>
<title>LDAP Administration Console -- Login</title>
<link href="ldapadmin.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</head>
<body>
<div id="TopBar" style="position:absolute; left:0px; top:0px; width:750px; height:215px; z-index:0"><img src="Images/t_l_corner.gif" width="152" height="234"><img src="Images/t_bar.gif" width="594" height="98" align="top"></div>
<div id="LDAPtext" style="position:absolute; left:215px; top:100px; width:532px; height:120px; z-index:0" class="header">LDAP Administration Console </div>
<div id="LoginBox" style="position:absolute; left:240px; top:270px; width:295px; height:180px; z-index:10; background-image: url(Images/bg_grid.gif); border:3px ridge #9999FF;">
  <p class="small_header" style="vertical-align: middle;">Login</p>
  <hr>
</div>
<div id="login_table" style="position:absolute; top: 335px; left: 255px; width: 270px; z-index: 20; height: 90px;">
  <form name="login_form" method="POST" >
  <table width="100%" style="vertical-align: middle;">
  <tr>
  <td width="6%">&nbsp;</td>
  <td width="34%" class="prompt">Username:</td>
  <td width="60%"><input name="authusername" type="text" size="25" maxlength="25"></td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
	<td class="prompt">Password:</td>
	<td><input name="password" type="password" size="25" maxlength="255"></td>
  </tr>
  <tr>
  <td colspan="3" width="80%" align="right"><input type="submit" name="login" value="  Submit  "></td>
  <td>&nbsp;</td>
  </tr>
  <tr>
  	<td colspan="2">
		<a style="align: top; font-size: 9px; color: #003399" 
		href="nosession.cgi">Forgot password?</a>
  </td>
  <td>
  [[ if(defined($main::invalid_pw)) { '<div class="small_link" style="color: rgb(255,15,15); text-decoration: none">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invalid Password </div>' } elsif (defined($main::expired)) { '<div class="small_link" style="color: rgb(255,15,15); text-decoration: none">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Session Expired </div>' } ]] 
  </td>
  </tr>
  </table>
  </form>
</div>
<div id="DropShadow" style="position:absolute; left:245px; top:275px; width:295px; height:180px; z-index:8; vertical-align: middle; background-color: #999999; padding: 3px"></div>

</body>
</html>
