<script language="javascript">
<!-- 
var DefaultHome = '[[ $main::config->{default_home} ]]';

var LDAPusers = new Object;

[[ join('',@main::userlist);  ]]

var LDAPgroups = new Object;

[[ join('',@main::grouplist); ]]

function populateForm(input, index) {
	var groupname = input.options[index].text;
	var forms = new Array;
	forms.ModGroupForm = true;
	forms.AddGroupForm = true;
	forms.DelGroupForm = true;
	for(obj in forms) {
		if(LDAPgroups[groupname] == null) {
			if(obj == "AddGroupForm") {
				document[obj].groupname.value="";
			} else {
				document[obj].groupname.value = "-----------------------";
			}
			document[obj].gid.value = "";
			continue;
		}
	
		document[obj].groupname.value = groupname;
		document[obj].gid.value = LDAPgroups[groupname].gid;
	}
}

function populateGroupSelect(input,idx) {
	var group = input.options[idx].text;
	var in_field = document.GroupMembersForm.userlist;
	var sel_field = document.GroupMembersForm.selected_userlist;
	var re = new RegExp;

	clearSelectBox(sel_field, in_field);
	if(group == '-----------------------') {
		return;
	}
	for(i=0;i<in_field.length;i++) {
		var testuser = in_field.options[i].text.replace(/^([a-zA-Z0-9]+) ---.*/, "$1");
		in_field.options[i].selected = false;
		if(LDAPgroups[group].memberUids[testuser] == true) {
			in_field.options[i].selected = true;
		}
	}

	populateSelectBox(in_field, sel_field);
}

function validateForm(input, field, grouptotest) {
	// Cursory check of field values to avoid wasting the admin's time, server validation is far more scrutinizing.
	if(field == null) {
		field='all';
	}
	
	var pass = new Object;
	var retval = true;

	pass.groupname = true;
	pass.gid = true;

	var groupname = input.groupname.value;
	var gid = input.gid.value;
	
	var re = new RegExp;
	
	for(group in LDAPgroups) {
		if(group == groupname) {
			if(grouptotest == group) {
				continue;
			} else {
				pass.groupname=false;
			}
		}
		if(LDAPgroups[group].gid == gid) {
			pass.gid=false;
		}
	}

	re.compile('^[a-zA-Z0-9]{1,8}$');
	if(! re.test(groupname)) {
		pass.groupname=false;
	}

	for(key in pass) {
		if(field != 'all') {
			if(key != field) {
				continue;
			}
		}
		if(pass[key] == false) {
			input[key].style.backgroundColor = '#ff0000';
			retval=false;
		} else {
			input[key].style.backgroundColor = null;
		}
	}
	return retval;
}
// -->

</script>

