<div id="users_sub_nav_menu" style="position:absolute; width:565px; height:45px; z-index:200; left: 165px; top: 110px; visibility: visible;">
		<table width="45%" border="0" cellspacing="0" cellpadding="4">
			<tr align="center">
				<td><a href="#" class="nav_item_small" onClick="swapLayers('modgroup');">Modify Groups</a></td>
				<td>|</td>
				<td><a href="#" class="nav_item_small" onClick="swapLayers('groupmembers');">Group Memberships</a></td>
				<td>|</td>
			</tr>
		</table>
		<hr>
</div>
  
  
<div id="modgroup" style="visibility: hidden; position:absolute; width:565px; height:385px; z-index:201; left: 165px; top: 130px;">
    <div style="border: 2px ridge #000099; padding: 0px; margin-top: 0px; visibility: inherit; position: absolute; width: 50px; height: 1px; top: 0; left: 21px; vertical-align: top; font-size: 0px;">		
	</div>
	
	<div style="visibility: inherit; position: absolute; width: 565px; height: 365px; top: 12px; left: 0px; z-index: 202">
	
		<form name="AddGroupForm" method="POST" action="index.cgi?session_id=[[ $main::params->{session_id} ]]&ssid=[[ $main::params->{ssid} ]]&screen=[[ $main::params->{screen}; ]]&action=addgroup" onSubmit="return validateForm(this);">
			<div style="visibility: inherit; position: absolute; width: 550px; height: 20px; top: 15px; left: 10px; z-index: 1000; background-color: #9999DA">
			Add Groups: 
			</div>
		
			<div style="visibility: inherit; position: absolute; width: 245px; height: 70px; top: 35px; left: 10px; z-index: 1000; border: 3px groove #9999FF">
			</div>
			<div style="visibility: inherit; position: absolute; width: 545px; height: 70px; top: 35px; left: 10px; z-index: 1000; border: 3px groove #9999FF">
				<div style="visibility: inherit; position: absolute; top: 10px; left: 10px; width: 200px;">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td style="font-size: 11px; font-weight: 900; color: #000099" width="50%">
								Group Name:<font style="color:#FF0000; font-weight: 900; font-size: 11px">*</font>  
							</td>
							<td>
								<input name="groupname" style="font-size: 9px" type="text" size="20" onchange="validateForm(document.AddGroupForm, 'groupname');">
							</td>
						</tr>
					</table>
				</div>
				<div style="visibility: inherit; position: absolute; padding: 0px; margin: 0px; width: 205px; height: 20px; top: 40px; left: 265px; z-index: 1000;">
					<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td style="font-size: 11px; font-weight: 900; color: #000099;" align="center">
								<input type="submit" value="  Add Group  " >
							</td>
							<td style="font-size: 11px; font-weight: 900; color: #000099;" align="center">
								<input type="reset" value="    Clear   ">
							</td>
						</tr>
					</table>
				</div>
			</div>
		</form>


		<form name="DelGroupForm" method="POST" action="index.cgi?session_id=[[ $main::params->{session_id} ]]&ssid=[[ $main::params->{ssid} ]]&screen=[[ $main::params->{screen}; ]]&action=delgroup" onSubmit="return validateForm(this, null, this.groupname.options[this.groupname.selectedIndex].text);"">
			<div style="visibility: inherit; position: absolute; width: 550px; height: 20px; top: 120px; left: 10px; z-index: 1000; background-color: #9999DA">
		Delete Groups: 
			</div>
		
			<div style="visibility: inherit; position: absolute; width: 245px; height: 70px; top: 140px; left: 10px; z-index: 1000; border: 3px groove #9999FF">
			</div>
			<div style="visibility: inherit; position: absolute; width: 545px; height: 70px; top: 140px; left: 10px; z-index: 1000; border: 3px groove #9999FF">
				<div style="visibility: inherit; position: absolute; top: 10px; left: 10px; width: 200px;">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td style="font-size: 11px; font-weight: 900; color: #000099" width="50%">
								Group Name:<font style="color:#FF0000; font-weight: 900; font-size: 11px">*</font>  
							</td>
							<td>
								<select name="groupname" style="font-size: 9px; width: 110px;" onchange="populateForm(this, selectedIndex);" onKeyUp="populateForm(this, selectedIndex);">
									<option value='-----------------------'>-----------------------</option>
									<script language="javascript">
										for ( key in LDAPgroups ) {	
											document.write('<option value="' + key + '">' + key + '</option>');
										}
									</script>
								</select>
								<script language="javascript">
									sortSelect(document.DelGroupForm.groupname);
								</script>
							</td>
						</tr>
					</table>
				</div>
				<div style="visibility: inherit; position: absolute; padding: 0px; margin: 0px; width: 205px; height: 20px; top: 40px; left: 265px; z-index: 1000;">
					<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td style="font-size: 11px; font-weight: 900; color: #000099;" align="center">
								<input type="submit" value="  Delete Group  " >
							</td>
							<td style="font-size: 11px; font-weight: 900; color: #000099;" align="center">
								<input type="reset" value="    Clear   ">
							</td>
						</tr>
					</table>
				</div>
			</div>
		</form>
       </div>
</div>


<!-- ################# ################# -->

<div id="groupmembers" style="visibility: hidden; position:absolute; width:565px; height:355px; z-index:201; left: 165px; top: 130px;">
	<div style="border: 2px ridge #000099; padding: 0px; margin-top: 0px; visibility: inherit; position: absolute; width: 50px; height: 1px; top: 0; left: 148px; vertical-align: top; font-size: 0px;">		
	</div>


	<form name="GroupMembersForm" method="post" action="index.cgi?session_id=[[ $main::params->{session_id} ]]&ssid=[[ $main::params->{ssid} ]]&screen=[[ $main::params->{screen} ]]&action=changemembers" onSubmit="selectAllSelectBox(document.GroupMembersForm.selected_userlist);">
  	<div style="visibility: inherit; position: absolute; width: 565px; height: 365px; top: 12px; left: 0px; z-index: 202">
		<div style="visibility: inherit; position: absolute; width: 545px; height: 270px; top: 10px; left: 10px; z-index: 1000; border: 3px groove #9999FF">
		
			<table border="0" cellpadding="2" cellspacing="2" width="100%">
				<tr>
					<td align="right">
						Select Group: 
					</td>
					<td align="left" colspan="2">
						<select name="groupname" style="font-size: 9px; width: 110px;" onChange="populateGroupSelect(this, selectedIndex);" onKeyUp="populateGroupSelect(this,selectedIndex);">
							<option value='-----------------------'>-----------------------</option>
							<script language="javascript">
								for ( key in LDAPgroups ) {	
									document.write('<option value="' + key + '">' + key + '</option>');
								}
							</script>
						</select>
						<script language="javascript">
							sortSelect(document.GroupMembersForm.groupname);
						</script>
					</td>
				</tr>
				<tr>
					<td align="center" style="font-size: 11px; font-weight: 900" colspan="3">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td align="center" style="font-size: 11px; font-weight: 900">
						All Users
					</td>
					<td>&nbsp;</td>
					<td align="center" style="font-size: 11px; font-weight: 900">
					 	Users in Group
					</td>
				</tr>
				<tr>
					<td style="font-size: 11px" align="left">
						<select name="userlist" multiple size="13" style="font-size: 9px; width: 220px">
							<script language="javascript">
								for ( key in LDAPusers ) { 
									if(LDAPusers[key].locked) { continue; }
									document.write('<option value="' + key + " ------ " + LDAPusers[key].firstname + " " + LDAPusers[key].lastname +'">' + key + " ------ " + LDAPusers[key].firstname + " " + LDAPusers[key].lastname + '</option>');
								}
							</script>
						</select>
					<script language="javascript">
						sortSelect(document.GroupMembersForm.userlist);
					</script>						
					</td>
					<td style="font-size: 11px" align="center">
						<input type="button" value="   &gt;&gt;   " onclick="populateSelectBox(document.GroupMembersForm.userlist, document.GroupMembersForm.selected_userlist);"><br>
						<input type="button" value="   &lt;&lt;   " onclick="populateSelectBox(document.GroupMembersForm.selected_userlist, document.GroupMembersForm.userlist);">
					</td>
					<td style="font-size: 11px" align="right">
						<select name="selected_userlist" multiple size="13" style="font-size: 9px; width: 220px">
						</select>
					</td>
				</tr>
				<tr>
					<td style="font-size: 11px" align="left" colspan="4">
					Regexp Filter: <input type="text" style="font-size: 9px; color: #000066" onKeyUp="filterSelectBox(this,event,document.GroupMembersForm.userlist,document.UnseenForm1.selectbox);">
					</td>
				</tr>
			</table>
		</div>
	</div>

	<div style="visibility: inherit; position: absolute; padding: 0px; margin: 0px; width: 245px; height: 20px; top: 340px; left: 265px; z-index: 1000;">

		<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td style="font-size: 11px; font-weight: 900; color: #000099;" align="center">
					<input type="submit" value="  Change Group Memberships  ">
				</td>
				<td style="font-size: 11px; font-weight: 900; color: #000099;" align="center">
					<input type="reset" value="    Clear   " onClick="clearSelectBox(document.GroupMembersForm.selected_userlist, document.GroupMembersForm.userlist);">
				</td>
			</tr>
		</table>
	</div>
	</form>
</div>

<div style="position: absolute; visibility: hidden; top: 0px; left: 600px; width: 200px; height: 200px; z-index: 15000;">
	<form name="UnseenForm1">
		<select multiple name="selectbox" size="10" style="width: 200px">
		</select>
	</form>
	<form name="UnseenForm2">
		<select multiple name="selectbox" size="10" style="width: 200px">
		</select>
	</form>
</div>
</body>
</html>
