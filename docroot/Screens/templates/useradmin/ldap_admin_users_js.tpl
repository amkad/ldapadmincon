<script language="javascript">
<!-- 
var DefaultHome = '[[ $main::config->{default_home} ]]';

var LDAPusers = new Object;

[[ join('',@main::userlist);  ]]

var LDAPgroups = new Object;

[[ join('',@main::grouplist); ]]

function updateHomedir(input, evt) {
   var typedText = input.value;
   var queryText = DefaultHome + typedText;
   
    if (typeof input.selectionStart != 'undefined') {
      if (evt.keyCode == 16) {
        return;
      }
	}
	document.AddUserInformation.homedir.value = queryText;
}

function populateForm(input, index) {
	var username = input.options[index].text;
	var divs = new Array;
	divs.ModUserInformation = true;
	divs.AddUserInformation = true;
	divs.DelUserInformation = true;
	for(obj in divs) {
		if(LDAPusers[username] == null) {
			if(obj == "AddUserInformation") {
				document[obj].username.value="";
			} else {
				document[obj].username.value = "-----------------------";
			}
			document[obj].email.value = "";
			document[obj].firstname.value = "";
			document[obj].lastname.value = "";
			document[obj].ldapadmin.checked = false;
			continue;
		}
	
		document[obj].username.value = username;
		document[obj].email.value = LDAPusers[username].email;
		document[obj].firstname.value = LDAPusers[username].firstname;
		document[obj].lastname.value = LDAPusers[username].lastname;
	
		if(LDAPusers[username].LDAPadmin) {
			if(LDAPusers[username].LDAPadmin == "true") {
				document[obj].ldapadmin.checked = true;
			} else {
				document[obj].ldapadmin.checked = false;
			}
		}
		if(LDAPusers[username].locked) {
			var locked_elem = getElemRefs('mod_locked');
			locked_elem.childNodes.item(0).data = "Account Locked";
			locked_elem.style.color = "#FF1515";

			locked_elem = getElemRefs('del_locked');
			locked_elem.childNodes.item(0).data = "Account Locked";
			locked_elem.style.color = "#FF1515";
		} else {
			var locked_elem = getElemRefs('mod_locked');
			locked_elem.childNodes.item(0).data = '';

			locked_elem = getElemRefs('del_locked');
			locked_elem.childNodes.item(0).data = '';
		}
	}
}



function validateForm(input, field, usertotest) {
	// Cursory check of field values to avoid wasting the user's time, server validation is far more scrutinizing.
	if(field == null) {
		field='all';
	}

	var pass = new Object;
	var retval = true;

	pass.username = true;
        pass.lastname = true;
	pass.firstname = true;
	pass.email = true;
	
	var username = input.username.value;
	var lastname = input.lastname.value;
        var firstname = input.firstname.value;
	var email = input.email.value;
	
	var re = new RegExp;
	
	for(user in LDAPusers) {
		if(user == username) {
			if(usertotest == user) {
				continue;
			} else {
				pass.username=false;
			}
		}
	}

	re.compile('^[a-zA-Z0-9]{1,14}$');
	if(! re.test(username)) {
		pass.username=false;
	}

	re.compile('^\\S+$');
	if (! re.test(lastname)) {
		pass.lastname = false;
	}
        if (! re.test(firstname)) {
                pass.firstname = false;
        }
	
	tld = '\\.com|\\.org|\\.net|\\.edu|\\.gov|';
	tld += '\\.ac|\\.ad|\\.ae|\\.af|\\.ag|\\.ai|\\.al|\\.am|\\.an|\\.ao|\\.aq|\\.ar|\\.as|\\.at|\\.au|';
	tld += '\\.aw|\\.az|\\.ax|\\.ba|\\.bb|\\.bd|\\.be|\\.bf|\\.bg|\\.bh|\\.bi|\\.bj|\\.bm|\\.bn|\\.bo|';
	tld += '\\.br|\\.bs|\\.bt|\\.bv|\\.bw|\\.by|\\.bz|\\.ca|\\.cc|\\.cd|\\.cf|\\.cg|\\.ch|\\.ci|\\.ck|';
	tld += '\\.cl|\\.cm|\\.cn|\\.co|\\.cr|\\.cs|\\.cu|\\.cv|\\.cx|\\.cy|\\.cz|\\.de|\\.dj|\\.dk|\\.dm|';
	tld += '\\.do|\\.dz|\\.ec|\\.ee|\\.eg|\\.eh|\\.er|\\.es|\\.et|\\.eu|\\.fi|\\.fj|\\.fk|\\.fm|\\.fo|';
	tld += '\\.fr|\\.ga|\\.gb|\\.gd|\\.ge|\\.gf|\\.gg|\\.gh|\\.gi|\\.gl|\\.gm|\\.gn|\\.gp|\\.gq|\\.gr|';
	tld += '\\.gs|\\.gt|\\.gu|\\.gw|\\.gy|\\.hk|\\.hm|\\.hn|\\.hr|\\.ht|\\.hu|\\.id|\\.ie|\\.il|\\.im|';
	tld += '\\.in|\\.io|\\.iq|\\.ir|\\.is|\\.it|\\.je|\\.jm|\\.jo|\\.jp|\\.ke|\\.kg|\\.kh|\\.ki|\\.km|';
	tld += '\\.kn|\\.kp|\\.kr|\\.kw|\\.ky|\\.kz|\\.la|\\.lb|\\.lc|\\.li|\\.lk|\\.lr|\\.ls|\\.lt|\\.lu|';
	tld += '\\.lv|\\.ly|\\.ma|\\.mc|\\.md|\\.mg|\\.mh|\\.mk|\\.ml|\\.mm|\\.mn|\\.mo|\\.mp|\\.mq|\\.mr|';
	tld += '\\.ms|\\.mt|\\.mu|\\.mv|\\.mw|\\.mx|\\.my|\\.mz|\\.na|\\.nc|\\.ne|\\.nf|\\.ng|\\.ni|\\.nl|';
	tld += '\\.no|\\.np|\\.nr|\\.nu|\\.nz|\\.om|\\.pa|\\.pe|\\.pf|\\.pg|\\.ph|\\.pk|\\.pl|\\.pm|\\.pn|';
	tld += '\\.pr|\\.ps|\\.pt|\\.pw|\\.py|\\.qa|\\.re|\\.ro|\\.ru|\\.rw|\\.sa|\\.sb|\\.sc|\\.sd|\\.se|';
	tld += '\\.sg|\\.sh|\\.si|\\.sj|\\.sk|\\.sl|\\.sm|\\.sn|\\.so|\\.sr|\\.st|\\.sv|\\.sy|\\.sz|\\.tc|';
	tld += '\\.td|\\.tf|\\.tg|\\.th|\\.tj|\\.tk|\\.tl|\\.tm|\\.tn|\\.to|\\.tp|\\.tr|\\.tt|\\.tv|\\.tw|';
	tld += '\\.tz|\\.ua|\\.ug|\\.uk|\\.um|\\.us|\\.uy|\\.uz|\\.va|\\.vc|\\.ve|\\.vg|\\.vi|\\.vn|\\.vu|';
	tld += '\\.wf|\\.ws|\\.ye|\\.yt|\\.yu|\\.za|\\.zm|\\.zw';

	
	re.compile('^[a-zA-Z][a-zA-Z\.\_0-9]+\@[a-zA-Z0-9][a-zA-Z0-9\.\-]{2,63}(' + tld + ')$');
	if( !re.test(email)) {
		pass.email = false;
	} 

	for(key in pass) {
		if(field != 'all') {
			if(key != field) {
				continue;
			}
		}
		if(pass[key] == false) {
			input[key].style.backgroundColor = '#ff0000';
			retval=false;
		} else {
			input[key].style.backgroundColor = null;
		}
	}
	return retval;
}
// -->

</script>

