










<div id="users_sub_nav_menu" style="position:absolute; width:565px; height:45px; z-index:200; left: 165px; top: 110px; visibility: visible;">
	<center>
		<table width="100%" border="0" cellspacing="0" cellpadding="4">
			<tr align="center">
				<td><a href="#" class="nav_item_small" onClick="swapLayers('addusers')">Add User</a></td>
				<td>|</td>
				<td><a href="#" class="nav_item_small" onClick="swapLayers('delusers');">Delete User</a></td>
				<td>|</td>
				<td><a href="#" class="nav_item_small" onClick="swapLayers('modusers');">Modify User</a></td>
				<td>|</td>
				<td><a href="#" class="nav_item_small" onClick="swapLayers('lockusers');">Lock User</a></td>
				<td>|</td>
				<td><a href="#" class="nav_item_small" onClick="swapLayers('passreset');">Password Resets</a></td>
			</tr>
		</table>
		<hr>
	</center>
</div>
  
  
<div id="addusers" style="visibility: visible; position:absolute; width:565px; height:385px; z-index:201; left: 165px; top: 130px;">
    <div style="border: 2px ridge #000099; padding: 0px; margin-top: 0px; visibility: inherit; position: absolute; width: 50px; height: 1px; top: 0; left: 13px; vertical-align: top; font-size: 0px;">		
	</div>
	<form name="AddUserInformation" method="POST" action="index.cgi?session_id=[[ $main::params->{session_id} ]]&ssid=[[ $main::params->{ssid} ]]&screen=[[ $main::params->{screen}; ]]&action=adduser" onSubmit="return validateForm(this);">
  	<div style="visibility: inherit; position: absolute; width: 565px; height: 365px; top: 12px; left: 0px; z-index: 202">
		<div style="visibility: inherit; position: absolute; width: 275px; height: 340px; top: 10px; left: 10px; z-index: 1000; border: 3px groove #9999FF">
			<table width="100%">
			<tr>
				<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
					Username:<font style="color:#FF0000; font-weight: 900; font-size: 11px">*</font>  
				</td>
				<td colspan="2">
					<input name="username" style="font-size: 9px" type="text" size="15" onchange="validateForm(document.AddUserInformation, 'username');">
				</td>
			</tr>
			<tr>
				<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
					First name:<font style="color:#FF0000; font-weight: 900; font-size: 11px">*</font>
				</td>
				<td colspan="2">
					<input name="firstname" style="font-size: 9px" type="text" size="15" onchange="validateForm(document.AddUserInformation, 'firstname');">
				</td>
			</tr>
			<tr>
				<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
					Last name:<font style="color:#FF0000; font-weight: 900; font-size: 11px">*</font>
				</td>
				<td colspan="2">
					<input name="lastname" style="font-size: 9px" type="text" size="15" onchange="validateForm(document.AddUserInformation, 'lastname');">
				</td>
			</tr>
			<tr>
			<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
				E-mail Address:<font style="color:#FF0000; font-weight: 900; font-size: 11px">*</font>
			</td>
			<td colspan="2">
				<input name="email" style="font-size: 9px" type="text" size="25" onchange="validateForm(document.AddUserInformation, 'email');">
			</td>
		</tr>
	</table>
	</div>
		<div style="visibility: inherit; position: absolute; width: 245px; height: 130px; top: 10px; left: 305px; z-index: 1000; border: 3px groove #9999FF">
  			<table width="100%">
				<tr>
					<td style="font-size: 11px; font-weight: 900; color: #000099;">
						E-mail temporary password to user?
					</td>
					<td style="font-size: 11px; font-weight: 900; color: #000099;">
						<input name='temppass' type="checkbox" checked>
					</td>
				</tr>
				<tr>
					<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
						<hr style="font-size: 11px; font-weight: 900; color: #000099; background-color: #000099">
					</td>
				</tr>
				<tr>
					<td style="font-size: 11px; font-weight: 900; color: #000099;">
						User is an LDAP Console Administrator?
					</td>
					<td style="font-size: 11px; font-weight: 900; color: #000099;">
						<input name='ldapadmin' type="checkbox">
					</td>
				</tr>
			</table>
		</div>		
  	</div>
	<div style="visibility: inherit; position: absolute; padding: 0px; margin: 0px; width: 245px; height: 20px; top: 340px; left: 305px; z-index: 1000;">
		<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td style="font-size: 11px; font-weight: 900; color: #000099;" align="center">
					<input type="submit" value="  Add User  " >
				</td>
				<td style="font-size: 11px; font-weight: 900; color: #000099;" align="center">
					<input type="reset" value="    Clear   ">
				</td>
			</tr>
		</table>
	</div>
	</form>
</div>

<!-- ############### DEL USERS LAYER ################## -->

<div id="delusers" style="visibility: hidden; position:absolute; width:565px; height:355px; z-index:201; left: 165px; top: 130px;">
	<div style="border: 2px ridge #000099; padding: 0px; margin-top: 0px; visibility: inherit; position: absolute; width: 50px; height: 1px; top: 0; left: 117px; vertical-align: top; font-size: 0px;">		
	</div>
	<form name="DelUserInformation" method="POST" action="?session_id=[[ $main::params->{session_id} ]]&ssid=[[ $main::params->{ssid} ]]&screen=[[ $main::params->{screen} ]]&action=deluser">
  	<div style="visibility: inherit; position: absolute; width: 565px; height: 365px; top: 12px; left: 0px; z-index: 202">
		<div style="visibility: inherit; position: absolute; width: 275px; height: 340px; top: 10px; left: 10px; z-index: 1000; border: 3px groove #9999FF">
			<table width="100%">
			<tr>
				<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
					Username:  
				</td>
				<td colspan="2">
				<select name="username" style="font-size: 9px;" onChange='populateForm(this, selectedIndex);' onKeyUp='populateForm(this,selectedIndex);'>
					<option value='-----------------------'>-----------------------</option>
					<script language="javascript">
					for ( key in LDAPusers ) { 
						document.write('<option value="' + key + '">' + key + '</option>');
					}
					</script>
				</select>
				<script language="javascript">
					sortSelect(document.DelUserInformation.username);
				</script>
				</td>
			</tr>
			<tr>
				<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
					First name:
				</td>
				<td colspan="2">
					<input name="firstname" style="font-size: 9px; background-color:#CFD0D8" type="text" size="15" readonly="true">
				</td>
			</tr>
			<tr>
				<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
					Last name:
				</td>
				<td colspan="2">
					<input name="lastname" style="font-size: 9px; background-color:#CFD0D8" type="text" size="15" readonly="true">
				</td>
			</tr>
			<tr>
			<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
				E-mail Address:
			</td>
			<td colspan="2">
				<input name="email" style="font-size: 9px; background-color:#CFD0D8" type="text" size="25" readonly="true">
			</td>
		</tr>
	</table>
	</div>
		<div style="visibility: inherit; position: absolute; width: 245px; height: 130px; top: 10px; left: 305px; z-index: 1000; border: 3px groove #9999FF">
  			<table width="100%">
				<tr>
					<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
						<hr style="font-size: 11px; font-weight: 900; color: #000099; background-color: #000099">
					</td>
				</tr>
				<tr>
					<td style="font-size: 11px; font-weight: 900; color: #000099;">
						User is an LDAP Console Administrator?
					</td>
					<td style="font-size: 11px; font-weight: 900; color: #000099;">
						<input name='ldapadmin' type="checkbox" disabled>
					</td>
				</tr>
				<tr>
					<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
						<hr style="font-size: 11px; font-weight: 900; color: #000099; background-color: #000099">
					</td>
				</tr>
				<tr>
					<td id="del_locked" style="font-size: 11px; font-weight: 900; color: #FF1515;">
						&nbsp;
					</td>
				</tr>
			</table>
		</div>		
  	</div>
	<div style="visibility: inherit; position: absolute; padding: 0px; margin: 0px; width: 245px; height: 20px; top: 340px; left: 305px; z-index: 1000;">
		<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td style="font-size: 11px; font-weight: 900; color: #000099;" align="center">
					<input type="submit" value="  Delete User  ">
				</td>
				<td style="font-size: 11px; font-weight: 900; color: #000099;" align="center">
					<input type="reset" value="    Clear   ">
				</td>
			</tr>
		</table>
	</div>
	</form>
</div>
 
 <!-- ############### MOD USERS LAYER ################# -->
 
<div id="modusers" style="visibility: hidden; position:absolute; width:565px; height:355px; z-index:201; left: 165px; top: 130px;">
	<div style="border: 2px ridge #000099; padding: 0px; margin-top: 0px; visibility: inherit; position: absolute; width: 50px; height: 1px; top: 0; left: 232px; vertical-align: top; font-size: 0px;">		
	</div>
	<form name="ModUserInformation" method="post" action="index.cgi?session_id=[[ $main::params->{session_id} ]]&ssid=[[ $main::params->{ssid} ]]&screen=[[ $main::params->{screen} ]]&action=moduser" onsubmit="return validateForm(this, null, this.username.options[this.username.selectedIndex].text);">
  	<div style="visibility: inherit; position: absolute; width: 565px; height: 365px; top: 12px; left: 0px; z-index: 202">
		<div style="visibility: inherit; position: absolute; width: 275px; height: 340px; top: 10px; left: 10px; z-index: 1000; border: 3px groove #9999FF">
			<table width="100%">
			<tr>
				<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
					Username:<font style="color:#FF0000; font-weight: 900; font-size: 11px">*</font>  
				</td>
				<td colspan="2">
				<select name="username" style="font-size: 9px" onChange='populateForm(this, selectedIndex);' onKeyUp='populateForm(this,selectedIndex);'>
					<option value='-----------------------'>-----------------------</option>
					<script language="javascript">
					for ( key in LDAPusers ) { 
						document.write('<option value="' + key + '">' + key + '</option>');
					}
					</script>
				</select>
				<script language="javascript">
					sortSelect(document.ModUserInformation.username);
				</script>
				</td>
			</tr>
			<tr>
				<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
					First name:<font style="color:#FF0000; font-weight: 900; font-size: 11px">*</font>
				</td>
				<td colspan="2">
					<input name="firstname" style="font-size: 9px" type="text" size="15" onchange="validateForm(document.ModUserInformation, 'firstname', document.ModUserInformation.username.text);">
				</td>
			</tr>
			<tr>
				<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
					Last name:<font style="color:#FF0000; font-weight: 900; font-size: 11px">*</font>
				</td>
				<td colspan="2">
					<input name="lastname" style="font-size: 9px" type="text" size="15"  onchange="validateForm(document.ModUserInformation, 'lastname', document.ModUserInformation.username.text);">
				</td>
			</tr>
			<tr>
			<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
				E-mail Address:<font style="color:#FF0000; font-weight: 900; font-size: 11px">*</font>
			</td>
			<td colspan="2">
				<input name="email" style="font-size: 9px" type="text" size="25" onchange="validateForm(document.ModUserInformation, 'email', document.ModUserInformation.username.text);">
			</td>
		</tr>
	</table>
	</div>
		<div style="visibility: inherit; position: absolute; width: 245px; height: 130px; top: 10px; left: 305px; z-index: 1000; border: 3px groove #9999FF">
  			<table id="boolean" width="100%">
				<tr>
					<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
						<hr style="font-size: 11px; font-weight: 900; color: #000099; background-color: #000099">
					</td>
				</tr>
				<tr>
					<td style="font-size: 11px; font-weight: 900; color: #000099;">
						User is an LDAP Console Administrator?
					</td>
					<td style="font-size: 11px; font-weight: 900; color: #000099;">
						<input name='ldapadmin' type="checkbox">
					</td>
				</tr>
				<tr>
					<td style="font-size: 11px; font-weight: 900; color: #000099" colspan="2">
						<hr style="font-size: 11px; font-weight: 900; color: #000099; background-color: #000099">
					</td>
				</tr>
				<tr>
					<td id="mod_locked" style="font-size: 11px; font-weight: 900; color: #FF1515;">
						&nbsp;
					</td>
				</tr>
			</table>
		</div>		
  	</div>
	<div style="visibility: inherit; position: absolute; padding: 0px; margin: 0px; width: 245px; height: 20px; top: 340px; left: 305px; z-index: 1000;">
		<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td style="font-size: 11px; font-weight: 900; color: #000099;" align="center">
					<input type="submit" value="  Modify User  ">
				</td>
				<td style="font-size: 11px; font-weight: 900; color: #000099;" align="center">
					<input type="reset" value="    Clear   ">
				</td>
			</tr>
		</table>
	</div>
	</form>
</div>

<!-- ################# LOCK USERS LAYER ################# -->

<div id="lockusers" style="visibility: hidden; position:absolute; width:565px; height:355px; z-index:201; left: 165px; top: 130px;">
	<div style="border: 2px ridge #000099; padding: 0px; margin-top: 0px; visibility: inherit; position: absolute; width: 50px; height: 1px; top: 0; left: 341px; vertical-align: top; font-size: 0px;">		
	</div>
	<form name="LockUserForm" method="post" action="index.cgi?session_id=[[ $main::params->{session_id} ]]&ssid=[[ $main::params->{ssid} ]]&screen=[[ $main::params->{screen} ]]&action=lockuser" onSubmit="selectAllSelectBox(document.LockUserForm.selected_userlist);">
  	<div style="visibility: inherit; position: absolute; width: 565px; height: 365px; top: 12px; left: 0px; z-index: 202">
		<div style="visibility: inherit; position: absolute; width: 545px; height: 270px; top: 10px; left: 10px; z-index: 1000; border: 3px groove #9999FF">
			<table border="0" cellpadding="2" cellspacing="2" width="100%">
				<tr>
					<td align="center" style="font-size: 11px; font-weight: 900">
						All Users
					</td>
					<td>&nbsp;</td>
					<td align="center" style="font-size: 11px; font-weight: 900">
					 Selected Users
					</td>
				</tr>
				<tr>
					<td style="font-size: 11px" align="left">
						<select name="userlist" multiple size="15" style="font-size: 9px; width: 220px">
							<script language="javascript">
								for ( key in LDAPusers ) { 
									if(LDAPusers[key].locked) { continue; }
									document.write('<option value="' + key + " ------ " + LDAPusers[key].firstname + " " + LDAPusers[key].lastname +'">' + key + " ------ " + LDAPusers[key].firstname + " " + LDAPusers[key].lastname + '</option>');
								}
							</script>
						</select>
					<script language="javascript">
						sortSelect(document.LockUserForm.userlist);
					</script>						
					</td>
					<td style="font-size: 11px" align="center">
						<input type="button" value="   &gt;&gt;   " onclick="populateSelectBox(document.LockUserForm.userlist, document.LockUserForm.selected_userlist);"><br>
						<input type="button" value="   &lt;&lt;   " onclick="populateSelectBox(document.LockUserForm.selected_userlist, document.LockUserForm.userlist);">
					</td>
					<td style="font-size: 11px" align="right">
						<select name="selected_userlist" multiple size="15" style="font-size: 9px; width: 220px">
						</select>
					</td>
				</tr>
				<tr>
					<td style="font-size: 11px" align="left" colspan="4">
					Regexp Filter: <input type="text" style="font-size: 9px; color: #000066" onKeyUp="filterSelectBox(this,event,document.LockUserForm.userlist,document.UnseenForm1.selectbox);">
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div style="position: absolute; top: 305px; left: 10px; color: #FF0000; font-size: 10px; font-weight: 900">
		To Unlock a user, Reset the user's password on the "Password Resets" tab
	</div>

	<div style="visibility: inherit; position: absolute; padding: 0px; margin: 0px; width: 245px; height: 20px; top: 340px; left: 305px; z-index: 1000;">

		<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td style="font-size: 11px; font-weight: 900; color: #000099;" align="center">
					<input type="submit" value="  Lock Users  ">
				</td>
				<td style="font-size: 11px; font-weight: 900; color: #000099;" align="center">
					<input type="reset" value="    Clear   " onClick="clearSelectBox(document.LockUserForm.selected_userlist, document.LockUserForm.userlist);">
				</td>
			</tr>
		</table>
	</div>
	</form>
</div>

<!-- ################# Password REset LAYER ################# -->

<div id="passreset" style="visibility: hidden; position:absolute; width:565px; height:355px; z-index:201; left: 165px; top: 130px;">
	<div style="border: 2px ridge #000099; padding: 0px; margin-top: 0px; visibility: inherit; position: absolute; width: 50px; height: 1px; top: 0; left: 467px; vertical-align: top; font-size: 0px;">		
	</div>
	<form name="PassResetForm" enctype="multipart/form-data" method="POST" action="index.cgi?session_id=[[ $main::params->{session_id} ]]&ssid=[[ $main::params->{ssid} ]]&screen=[[ $main::params->{screen}; ]]&action=passreset" onSubmit="selectAllSelectBox(document.PassResetForm.selected_userlist);">
  	<div style="visibility: inherit; position: absolute; width: 565px; height: 365px; top: 12px; left: 0px; z-index: 202">
		<div style="visibility: inherit; position: absolute; width: 545px; height: 270px; top: 10px; left: 10px; z-index: 1000; border: 3px groove #9999FF">
			<table border="0" cellpadding="2" cellspacing="2" width="100%">
				<tr>
					<td align="center" style="font-size: 11px; font-weight: 900">
						All Users
					</td>
					<td>&nbsp;</td>
					<td align="center" style="font-size: 11px; font-weight: 900">
					 Selected Users
					</td>
				</tr>
				<tr>
					<td style="font-size: 11px" align="left">
						<select name="userlist" multiple size="15" style="font-size: 9px; width: 220px">
							<script language="javascript">
								for ( key in LDAPusers ) { 
									document.write('<option value="' + key + " ------ " + LDAPusers[key].firstname + " " + LDAPusers[key].lastname +'">' + key + " ------ " + LDAPusers[key].firstname + " " + LDAPusers[key].lastname + '</option>');
								}
							</script>
						</select>
					<script language="javascript">
						sortSelect(document.PassResetForm.userlist);
					</script>
					</td>
					<td style="font-size: 11px" align="center">
						<input type="button" value="   &gt;&gt;   " onclick="populateSelectBox(document.PassResetForm.userlist, document.PassResetForm.selected_userlist);"><br>
						<input type="button" value="   &lt;&lt;   " onclick="populateSelectBox(document.PassResetForm.selected_userlist, document.PassResetForm.userlist);">
					</td>
					<td style="font-size: 11px" align="right">
						<select name="selected_userlist" multiple size="15	" style="font-size: 9px; width: 220px">
						</select>
					</td>
				</tr>
				<tr>
					<td style="font-size: 11px" align="left" colspan="4">
					Regexp Filter: <input type="text" style="font-size: 9px; color: #000066" onKeyUp="filterSelectBox(this,event,document.PassResetForm.userlist,document.UnseenForm2.selectbox);">
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div style="visibility: inherit; position: absolute; padding: 0px; margin: 0px; width: 245px; height: 20px; top: 340px; left: 305px; z-index: 1000;">
		<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td style="font-size: 11px; font-weight: 900; color: #000099;" align="center">
					<input type="submit" value="  Reset Passwords  ">
				</td>
				<td style="font-size: 11px; font-weight: 900; color: #000099;" align="center">
					<input type="reset" value="    Clear   " onClick="clearSelectBox(document.PassResetForm.selected_userlist, document.PassResetForm.userlist);">
				</td>
			</tr>
		</table>
	</div>
	</form>
</div>

<div style="position: absolute; visibility: hidden; top: 0px; left: 600px; width: 200px; height: 200px; z-index: 15000;">
	<form name="UnseenForm1">
		<select multiple name="selectbox" size="10" style="width: 200px">
		</select>
	</form>
	<form name="UnseenForm2">
		<select multiple name="selectbox" size="10" style="width: 200px">
		</select>
	</form>
</div>
</body>
</html>
