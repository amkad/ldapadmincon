<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>LDAP Administration Console</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function compareText (option1, option2) {
  return option1.text < option2.text ? -1 :
    option1.text > option2.text ? 1 : 0;
}
function compareValue (option1, option2) {
  return option1.value < option2.value ? -1 :
    option1.value > option2.value ? 1 : 0;
}
function compareTextAsFloat (option1, option2) {
  var value1 = parseFloat(option1.text);
  var value2 = parseFloat(option2.text);
  return value1 < value2 ? -1 :
    value1 > value2 ? 1 : 0;
}
function compareValueAsFloat (option1, option2) {
  var value1 = parseFloat(option1.value);
  var value2 = parseFloat(option2.value);
  return value1 < value2 ? -1 :
    value1 > value2 ? 1 : 0;
}
function sortSelect (select, compareFunction) {
  if (!compareFunction)
    compareFunction = compareText;
  var options = new Array (select.options.length);
  for (var i = 0; i < options.length; i++)
    options[i] = 
      new Option (
        select.options[i].text,
        select.options[i].value,
        select.options[i].defaultSelected,
        select.options[i].selected
      );
  options.sort(compareFunction);
  select.options.length = 0;
  for (var i = 0; i < options.length; i++)
    select.options[i] = options[i];
}


var cur_lyr;	// holds id of currently visible layer
function swapLayers(id) {
  if (cur_lyr) hideLayer(cur_lyr);
  showLayer(id);
  cur_lyr = id;
}

function showLayer(id) {
  var lyr = getElemRefs(id);
  if (lyr && lyr.css) lyr.css.visibility = "visible";
}

function hideLayer(id) {
  var lyr = getElemRefs(id);
  if (lyr && lyr.css) lyr.css.visibility = "hidden";
}

function getElemRefs(id) {
	var el = (document.getElementById)? document.getElementById(id): (document.all)? document.all[id]: (document.layers)? document.layers[id]: null;
	if (el) el.css = (el.style)? el.style: el;
	return el;
}

function populateSelectBox(field1, field2) {
	var j=field2.length;
	for(i=0;i<field1.length;i++) {
		if(field1.options[i].selected) {
			var Duplicate=false;
   			for(k=0;k<field2.length;k++) {
				if(field1.options[i].text == field2.options[k].text) {
					Duplicate = true;
					break;
				}
	        }
			if(Duplicate == false) { 
				newOpt=field1.options[i].text
				field2.length=j+1;
				field2.options[j]= new Option(newOpt,newOpt)
				j++;
			}
		}
	}
	for( i=field1.length - 1; i>=0; i--) {	
		if(field1.options[i].selected) {
			field1.options[i] = null;
		}
	}
	sortSelect(field1);
	sortSelect(field2);
}

function filterSelectBox(input, evt, field1, field2) {
	var this_filter=input.value;
	var len = this_filter.length
	/*if(evt.keyCode > 65 + 26 || evt.keyCode < 65) {
	  	if(evt.keyCode != 8) {
			return;
		}
	} */
	
	if(this_filter.length < 1) {
		clearSelectBox(field2,field1);
		return;
	}
	
	var in_re = this_filter;
	
	re = new RegExp(in_re, 'i');

	for(i=0;i<field2.length;i++) {
		field2.options[i].selected=false;
		if(re.test(field2.options[i].value)) {
			field2.options[i].selected=true;
		}
	}
	populateSelectBox(field2,field1); 		

	for(i=0;i<field1.length;i++) {
		field1.options[i].selected=true;
		if(re.test(field1.options[i].value)) {
			field1.options[i].selected=false;
		}
	}
	populateSelectBox(field1,field2);
	for(i=0;i<field1.length;i++) {
		field1.options[i].selected=false;
	}
}

function clearSelectBox(field1, field2) {
	if(! field2) {
		field1.length=0;
	}
	else {
		for(i=0;i<field1.length;i++) {
			field1.options[i].selected=true;
		}
		populateSelectBox(field1,field2);
	}
}

function selectAllSelectBox(field1) {
	for(i=0;i<field1.length;i++) {
		field1.options[i].selected=true;
	}
}


// Refresh timer for client-side notification of session expiration
function session_timeout() {
	var uniq = new Date();
	uniq = uniq.getTime();
	window.location = 'index.cgi?session_id=[[ $main::params->{session_id} ]]&ssid=[[ $main::params->{ssid} ]]&' + uniq;
}
setTimeout('session_timeout()', (((60 * 15) + 2) * 1000));

//-->
</script>



