#!/usr/local/bin/perl -w 


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################


use strict;
{

    package Screens;
    use Modules qw(Use Load);
    use Utils;

    #use CGI qw(:all);

    sub AUTOLOAD {
        my $basename;
        our $AUTOLOAD;
        ( $basename = $AUTOLOAD ) =~ s/.*:://g;
        $basename =~ s/(\(|\))//g;
        $AUTOLOAD =~ s/(\(|\))//g;
        $AUTOLOAD =~ s/\:\:/\//g;
        my $module = $AUTOLOAD;
        $AUTOLOAD = undef;

        $basename .= "Screen" if ( $basename !~ m/Screen$/ );
        require "$module.pm";
        my $sub = \&{$basename};
        goto &$sub if ( ref($sub) eq "CODE" );
    }
}
1;

