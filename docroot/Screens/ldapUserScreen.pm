#!/usr/local/bin/perl -w


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################



use Templates qw(tpl);
use UserPlugIns::User;

sub ldapUserScreen(@) {
    my @retval;

    my $uadmin = UserPlugIns::User->new();

    my %userhash = $uadmin->ThisUser();
    for ( keys %userhash ) {
        push @main::userlist, "LDAPusers.$_ = new Object;\n";
        for my $val ( keys %{ $userhash{$_} } ) {
            push @main::userlist,
              "LDAPusers.$_.$val = '" . $userhash{$_}->{$val} . "';\n";
        }
        push @main::userlist, "\n";
    }

    $main::default_layer =
      'user_information\'); populateForm(\'' . $main::params->{authusername};

    @retval = tpl("ldap_admin_template.tpl");
    push @retval, tpl("user/ldap_user_js.tpl");
    push @retval, tpl("ldap_admin_shell.tpl");
    push @retval, tpl("user/ldap_user_template.tpl");
    return @retval;
}
1;
