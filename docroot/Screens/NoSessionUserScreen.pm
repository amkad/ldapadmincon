#!/usr/local/bin/perl -w


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################



use Templates qw(tpl);

sub NoSessionUserScreen(@) {
    my @retval;

    $main::default_layer        = 'enteremail';
    $main::params->{session_id} = 'undef';
    $main::params->{ssid}       = 'undef';

    @retval = tpl("ldap_admin_template.tpl");
    push @retval, tpl("ldap_admin_shell.tpl");
    push @retval, tpl("nosession/email_dialog.tpl");

    return @retval;
}
1;
