#!/usr/local/bin/perl -w


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################



use Templates qw(tpl);
use PlugIns::UserAdmin;
use PlugIns::GroupAdmin;

sub ldapGroupAdminScreen(@) {
    my @retval;

    my $uadmin = PlugIns::UserAdmin->new();
    my $gadmin = PlugIns::GroupAdmin->new();

    @main::userlist  = ();
    @main::grouplist = ();

    my %userhash = $uadmin->AllUsers();
    for ( keys %userhash ) {
        push @main::userlist, "LDAPusers.$_ = new Object;\n";
        for my $val ( keys %{ $userhash{$_} } ) {
            next unless ( $val =~ m/firstname/ || $val =~ m/lastname/ );
            push @main::userlist,
              "LDAPusers.$_.$val = '" . $userhash{$_}->{$val} . "';\n";
        }
        push @main::userlist, "\n";
    }

    my %grouphash = $gadmin->AllGroups();
    for ( keys %grouphash ) {
        push @main::grouplist, "LDAPgroups['$_'] = new Object;\n";
        for my $val ( keys %{ $grouphash{$_} } ) {
            $val =~ /^users$/ && do {
                push @main::grouplist,
                  "LDAPgroups['$_'].memberUids = new Array();\n";
                for my $member ( @{ $grouphash{$_}->{$val} } ) {
                    push @main::grouplist,
                      "LDAPgroups['$_'].memberUids.$member = true;\n";
                }
                next;
            };
            push @main::grouplist,
              "LDAPgroups['$_'].$val = '" . $grouphash{$_}->{$val} . "';\n";
        }
        push @main::grouplist, "\n";
    }

    $main::default_layer = 'modgroup';

    @retval = tpl("ldap_admin_template.tpl");
    push @retval, tpl("groupadmin/ldap_admin_group_js.tpl");
    push @retval, tpl("ldap_admin_shell.tpl");
    push @retval, tpl("groupadmin/ldap_admin_group_template.tpl");
    return @retval;
}
1;
