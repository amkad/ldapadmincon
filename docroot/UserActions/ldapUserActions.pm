#!/usr/local/bin/perl -w


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################


{

    package UserActions::ldapUserActions;
    use strict;
    use Utils qw(basename fgrep);
    use UserPlugIns::User;
    use Screens;

    our $params;
    our $Uadmin;
    our $Gadmin;
    our %opts;

    sub new {
        my $proto = shift;
        %opts = @_;

        my $class = ref($proto) || $proto;
        my $self;
        eval { $self = $class->SUPER::new(); };

        $self->{changepassword} = \&changepassword;

        bless( $self, $class );

        $params = $main::params;
        $Uadmin = UserPlugIns::User->new();

        return $self;
    }

    sub VerifyPassword(@) {
        my $self         = shift;
        my $password     = shift;
        my $specialchars =
          '\!\@\#\$\%\^\&\*\(\)\[\]\{\}\.\,\<\>\?\/\\\|\~\`\:\;\'\"';
        my $num = { upper => 0, lower => 0, digits => 0, special => 0 };

        my $minlength  = $main::config->{pass_strength}->{MinLength};
        my $minupper   = $main::config->{pass_strength}->{RequireUpper};
        my $minlower   = $main::config->{pass_strength}->{RequireLower};
        my $mindigits  = $main::config->{pass_strength}->{RequireDigits};
        my $minspecial = $main::config->{pass_strength}->{RequireSpecial};

        unless ($password) {
            push @main::errors, "Password not provided\n";
            return undef;
        }

        if ( length($password) < $minlength ) {
            push @main::errors,
"Password too short, password must be at least $minlength characters long";
            return undef;
        }

        for ( my $i = 0 ; $i < length($password) ; $i++ ) {
            my $testchar = substr( $password, $i, 1 );
            $testchar =~ m/[A-Z]/           && $num->{upper}++;
            $testchar =~ m/[a-z]/           && $num->{lower}++;
            $testchar =~ m/[0-9]/           && $num->{digits}++;
            $testchar =~ m/[$specialchars]/ && $num->{special}++;
        }

        my $message = sub {
            my $name = shift;
            my $min  = shift;

            return
              "Password does not contain the minumum of $min $name characters";
        };

        push @main::errors, &$message( "upper case", $minupper )
          if ( $num->{upper} < $minupper );
        push @main::errors, &$message( "lower case", $minlower )
          if ( $num->{lower} < $minlower );
        push @main::errors, &$message( "digit", $mindigits )
          if ( $num->{digits} < $mindigits );
        push @main::errors, &$message( "special", $minspecial )
          if ( $num->{special} < $minspecial );

        return undef if ( scalar @main::errors > 0 );
        return 1;
    }

    sub changepassword(@) {
        my $self = shift;

        unless ( $self->VerifyPassword( $main::params->{newpassword2} ) ) {
            print Screens::Status();
            exit 1;
        }

        if (    $main::params->{newpassword1}
             && $main::params->{newpassword2}
             && $main::params->{newpassword1} eq $main::params->{newpassword2} )
        {

            eval {
                $Uadmin->ChangePassword( $main::params->{authusername},
                                         $main::params->{newpassword2} );
            };
            if ($@) {
                push @main::errors, "User: $main::params->{authusername}: $@";
            }
        }

        $main::action_string = 'Password changed';
        print Screens::Status();
        exit 0;
    }
}
1;
