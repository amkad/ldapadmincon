#!/usr/local/bin/perl -w


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################



require "config.ph";

{
    $main::userplugins{User}->{Screen}  = 'ldapUserScreen';
    $main::userplugins{User}->{Actions} = 'ldapUserActions';
    $main::userplugins{User}->{index}   = 0;

    package UserPlugIns::User;
    use LDAPtools qw/BindLDAP makeDN UpdateAttr parseURI/;
    use Utils qw(basename);
    use Net::LDAP;
    use Net::LDAPS;
    use strict;
    use Data::Dumper;
    use POSIX;
    use Email;

    our $ldap;
    our %opts;

    sub new {
        my $proto = shift;
        %opts = @_;

        my $class = ref($proto) || $proto;
        my $self;
        eval { $self = $class->SUPER::new(); };

        $ldap = $opts{ldap} || BindLDAP();

        $self->{ChangePassword} = undef;
        $self->{UpdateUserAttr} = undef;

        bless( $self, $class );

        return $self;
    }

    sub ChangePassword(@) {
        my $self         = shift;
        my $username     = shift;
        my $new_password = shift;
        my %args         = @_;
        my $mesg;

        chomp( my $crypted_passwd =
`$main::config->{slappasswd} -h $main::config->{cryptmethod} -s '$new_password'`
        );

        my $days_since_epoch = POSIX::ceil( time() / 60 / 60 / 24 );

        my $dn = makeDN($username);

        if ($ldap) {
            $mesg = $ldap->search( base   => $main::config->{base_passwd},
                                   filter => "(uid=$username)" );
            my $locked = undef;
            for my $entry ( $mesg->all_entries() ) {
                $locked = 1
                  if ( $entry->exists('uid')
                       && !$entry->exists('userPassword') );
            }

            if ($locked) {
                die "Account locked, contact your system administrator\n";
            }

            $mesg = $ldap->modify(
                       $dn,
                       changes => [
                           replace => [ userPassword     => $crypted_passwd ],
                       ]
            );

            $mesg->code() && die $mesg->error() . "\n";
            return 1;
        }
    }

    sub ThisUser(@) {
        my $self = shift;
        my %args = @_;
        my %rethash;

        my $mesg = $ldap->search(base   => $main::config->{base_passwd},
                                 filter => "(uid=$main::params->{authusername})"
                                );

        for my $entry ( $mesg->all_entries() ) {
            my $username = $entry->get_value('uid');
            $rethash{$username} = {
                                    firstname => $entry->get_value('givenName'),
                                    lastname  => $entry->get_value('sn'),
                                  };
            my $gecos;
            my $LDAPadmin;
            my $email;

            $rethash{$username}->{locked} = 1
              if ( !$entry->exists('userPassword') );

            $rethash{$username}->{gecos} = $gecos
              if ( $gecos = $entry->get_value('cn') );
            $rethash{$username}->{LDAPadmin} = $LDAPadmin
              if ( $LDAPadmin = $entry->get_value('LDAPadmin') );
            $rethash{$username}->{email} = $email
              if ( $email = $entry->get_value('mail') );
        }
        return %rethash;
    }

}

1;
