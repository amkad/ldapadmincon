#!/usr/local/bin/perl -w


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################


use strict;

our $BASE_INSTALL_PATH;
BEGIN {
	$BASE_INSTALL_PATH='/opt/ldapadmin';
	unshift @INC,   '/etc/LDAPadmin';
	unshift @INC, 	$BASE_INSTALL_PATH, 
					$BASE_INSTALL_PATH . "/Lib", 
					$BASE_INSTALL_PATH . "/Screens",
					$BASE_INSTALL_PATH . "/PlugIns";
}

$main::config = {
	# Configuration root
	auth_mech 		=> "LDAP",
	install_dir 	=> $BASE_INSTALL_PATH,
	STATE_DIR 		=> "/var/tmp/STATE",

	# Password strength rules:
	pass_strength	=> {  
			RequireUpper 	=> 2, #min number of upper case chars
			RequireLower	=> 2, #min number of lower case chars
			RequireSpecial	=> 1, #min number of "special" chars
			RequireDigits	=> 1, #min number of digit chars
			MinLength		=> 8, #min password length
	},

	# Base plugins that all ou=People,dc=example,dc=com (base_passwd)
	base_passwd 	=> "ou=People,dc=example=com",
	base_group		=> "ou=Group,dc=dc=example,dc=com",
	uid_attr		=> "uid",
	uri				=> "ldaps://ldap-server.fqdn.example.com:636/",
	cryptmethod		=> "{ssha}",

## TODO:
# remove the dependency of slappasswd, and either figure out how to make OpenLDAP
# and/or Net::LDAPS perform the encryption in OpenLDAP (using the default cryptmethod
# specified in the slapd.conf...) or hash the string in native perl (leaning heavily
# toward the former, as a the repetative "cryptmethod" definition annoys me to no end.

	slappasswd		=> "/usr/sbin/slappasswd",
	default_home	=> '/export/home/', ##must end with a '/'
	skel_dir		=> '/etc/skel/',

	# LDAP account details, account must have read/write permissions to all managed attributes
	adminbinddn		=> "cn=Manager,dc=example,dc=com",

## TODO:
# Want to support SASL/EXTERNAL bind method, so the x509 key (in pem format)  provided below
# will be used for authentication in lieu of a plaintext password. Just couldn't figure out the 
# Authen::SASL module... help wanted. //would still offer a adminbindpw option in the config, in
# addition to a adminkeypw option, for des encrypted pem's

	adminbindpw  	=> 'secret',
	clientcert 		=> "/etc/openldap/ssl/ldapclient.cert",
	clientkey  		=> "/etc/openldap/ssl/ldapclient.pem",
	cacert	   		=> "/etc/openldap/ssl/CA/cacert.pem",

	# Email server details
	smtp_server		=> 'smtp-server.example.com',
	hostname		=> 'example.com', #hostname to put in X-sender field
	docroot			=> '/ldapadmin',
	email_message	=> '/etc/LDAPadmin/email_message',
	email_subject	=> 'New LDAP account or LDAP password reset',

	# SUDOer's plugin ## Not implemented yet
	sudoersbase		=> "ou=SUDOers,dc=example,dc=com",
}
