#!/usr/local/bin/perl -wT 


# ###########################################################################
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or visit http://www.gnu.org/copyleft/gpl.html
#
# ###########################################################################


{
	package main;
	use strict;
	our $config;

	my ($uid,$gid,$homedir,$session_key,$ssid);
	BEGIN {
		# Include config.sh so LDWsession is in INC;
		require "/etc/LDAPadmin/config.ph";
	
		# Clean env
		%ENV = ();
		$ENV{PATH} = '/bin:/usr/bin';
	
		$uid = $ARGV[0] || ""; 
		$gid = $ARGV[1] || "";
		$homedir = $ARGV[2] || "";
		$session_key = $ARGV[3] || "";
		$ssid = $ARGV[4] || "";
	
		# Taint clean incomming variables
	
		$uid =~ m/^([0-9]+)$/;
		$uid = $1;
	
		$gid =~ m/^([0-9]+)$/;
		$gid = $1;
	
		$homedir =~ m/^(\/[A-Za-z0-9\_\-\/]+)$/;
		$homedir = $1;
	
		$session_key =~ m/^([A-Za-z0-9]+)$/;
		$session_key = $1;
	
		$ssid =~ m/^([A-Za-z0-9\%]+)$/;
		$ssid = $1;
	}

	use LDWSession;
	use Utils qw(fgrep basename);

	my $ses=LDWSession->new();
	
	if($ses->valid($session_key, $ssid)) {
		if(fgrep('^userisAdmin=true$', "$main::config->{STATE_DIR}/$session_key")) {
			unless(-d $homedir) {
				print STDERR "homedir does not exist\n";
				exit 1;
			}
			
			unless($main::config->{skel_dir} && -d $main::config->{skel_dir}) {
				print STDERR "No skel_dir defined in config, or skel_dir does not exist\n";
				exit 1;
			}

			system("cp -rp $main::config->{skel_dir}/. $homedir");
			system("chown -R $uid:$gid $homedir");
			exit 0;
		}
	}
	print STDERR "Un-Authorized\n";
}
